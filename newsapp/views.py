from django.shortcuts import render, redirect
from django.http import *
from django.core.mail import send_mail
from django.contrib import messages
from helper import * # importing all the methods from helper.py module
from .models import (Menu, MenuContent, ImageSlider, QuickLinks, SocialLinks, Horoscope)
import requests
import datetime
from .form import *
from django.template.loader import render_to_string # required to render the email template in html format
from django.core.paginator import Paginator #for pagination
from django.db.models import Q
import json
from django.conf import settings # for importing settings constant variables
# Create your views here.

def index(request):
    pop_query = Popup.objects.filter(status = True).order_by('order_by')
    count_popup = pop_query.count()

    data = {
        'tab_title': 'Home',
        'slider_img': ImageSlider.objects.filter(status = True),
        'fashion_content': MenuContent.objects.filter(status = True, menu_id = 12).order_by('-id')[:5],
        'technology_content': MenuContent.objects.filter(status = True, menu_id = 11).order_by('-id')[:5],
        'business_content': MenuContent.objects.filter(status = True, menu_id = 10).order_by('-id')[:5],
        'sports_content': MenuContent.objects.filter(status = True, menu_id = 9).order_by('-id')[:5],
        'photography_content': MenuContent.objects.filter(status = True, menu_id = 14).order_by('-id')[:6],
        'sponsors': Sponsor.objects.filter(status = True).order_by('order_by'),
        'popup': pop_query,
        'count_popup': count_popup,
    }
    return render(request, page('home/home'), data)


def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            from_email = form.cleaned_data['email']
            to_email = ['ujjwalshrestha1996@gmail.com']
            
            # For ReCaptcha
            clientKey = request.POST.get('g-recaptcha-response')
            secretKey = settings.GOOGLE_RECAPTCHA_SECRET_KEY,
            captcha_data = {
                'secret': secretKey,
                'response': clientKey
            }
            s = requests.session()
            r = s.post('https://www.google.com/recaptcha/api/siteverify', data=captcha_data) # this request is in json format
            response = json.loads(r.text) # converting the json format to python dictionary
            
            # Checking if the response from captcha is true or not.
            if response['success']:
                html_msg = render_to_string(page('email_template/contact_email_temp'), {
                    'name': name,
                    'subject': subject,
                    'message': message,
                    'from_email': from_email,
                    'to_email': to_email,
                })
                send_mail(subject, message, 'NewsFeed', to_email, fail_silently = True, html_message = html_msg)
                
                alert_msg = 'Thank You For Feedback.'
                messages.success(request, alert_msg)
                return redirect('contact')
            else:
                alert_msg = 'Invalid Captcha. Please try again.'
                messages.error(request, alert_msg)
                return redirect('contact')
        
    else:        
        form = ContactForm
    
    data = {
        'tab_title': 'Contact',
        'contact_form': form
    }
    return render(request, page('contact/contact'), data)


def horoscope(request, slug):
    menu_data = Menu.objects.get(slug=slug) # getting data from Menu table through slug.
    data = {    
        'tab_title': menu_data.menu_name,
        'title': menu_data.menu_name,
        'content': Horoscope.objects.filter(status = True).order_by('order_by')
    }
    
    get_page = page('horoscope/horoscope')
    return render(request, get_page, data)


def menu_content(request, slug):
    menu_data = Menu.objects.get(slug=slug) # getting data from Menu table through slug.
    menu_id = menu_data.id # ID of the respective menu.
    page_type = menu_data.page_type # Page type of the respective menu.
    
    # try/catch to handle the existence of MenuContent for the respective menu_id
    try: 
        #detail page does not require pagination  
        if page_type == 'dp' or page_type == 'gp':
            # getting contents of the respective menu through the menu_id.
            content = MenuContent.objects.filter(menu_id = menu_id, status = True).order_by('-created_at') 
        
        else:
            #for pagination in the list
            content_list = MenuContent.objects.filter(menu_id = menu_id, status = True).order_by('-created_at') 
            paginator = Paginator(content_list, 5) # Show 5 contents per page

            pages = request.GET.get('page')
            content = paginator.get_page(pages)
                    
    except MenuContent.DoesNotExist:
        content = None
    
    data = {    
        'tab_title': menu_data.menu_name,
        'title': menu_data.menu_name,
        'content': content
    }
    
    if slug == 'daily-horoscope':
        return horoscope(request, slug)
    
    elif slug == 'contact':
        return contact(request)
    
    elif slug == 'faq':
        return faq(request)
    
    elif page_type == 'dp': # For detail page type
        #For updating the visited number
        # if MenuContent.objects.get(menu_id = menu_id, status = True).exists():
        # mc = MenuContent.objects.get(menu_id = menu_id, status = True)
        # mc.visited += 1
        # mc.save()
        
        get_page = page('menu-content/detail')
        
    elif page_type == 'slp': # For simple listing page type
        get_page = page('menu-content/simple-list')
        
    elif page_type == 'tlp': # For thumbnail listing page type
        get_page = page('menu-content/thumbnail-list')
        
    elif page_type == 'gp': # For gallery page type
        get_page = page('menu-content/gallery') 
        
    elif page_type == 'clp': # For collapsible page type
        get_page = page('menu-content/collapse')
         
    return render(request, get_page, data)


def details(request, slug):
    #For updating the visited number
    mc = MenuContent.objects.get(slug = slug, status = True)
    mc.visited += 1
    mc.save()
    
    try: # try/catch to handle the existence of MenuContent for the respective menu_id
        content = MenuContent.objects.filter(slug = slug, status = True) # getting contents of the respective menu through the menu_id.
    except MenuContent.DoesNotExist:
        content = None
        
    data = {
        # 'tab_title': content[0].menu_id,
        'title': content[0].title,
        'content': content
    }
    
    return render(request, page('menu-content/detail'), data)


def horoscope_detail(request, param):
    get_api = "http://horoscope-api.herokuapp.com/horoscope/today/" + param
    horoscope = requests.get(get_api).json()
    
    get_data = Horoscope.objects.get(name=param)
    
    data = {
        'tab_title': 'Daily Horoscope',
        'title': param,
        'date': datetime.date.today(),
        'info': get_data,
        'content': horoscope
    }
    
    return render(request, page('horoscope/detail'), data)

def slider_detail(request, slug):
    try: # try/catch to handle the existence of SliderContent for the respective slug
        content = ImageSlider.objects.filter(slug = slug, status = True) # getting contents of the respective Slider through the slug.
    except ImageSlider.DoesNotExist:
        content = None
        
    data = {
        # 'tab_title': content[0].menu_id,
        'title': content[0].title,
        'content': content
    }
    
    return render(request, page('home/img_slider_detail'), data)


def faq(request):
    try: # try/catch to handle the existence of SliderContent for the respective slug
        content = Faq.objects.filter(status = True).order_by('order_by') # getting contents of the respective Slider through the slug.
    except Faq.DoesNotExist:
        content = None
        
    data = {
        # 'tab_title': content[0].menu_id,
        'title': 'Frequently Asked Questions',
        'content': content
    }
    
    return render(request, page('faq/faq'), data)


def sponsors(request):
    try: # try/catch to handle the existence of SliderContent for the respective slug
        content = Sponsor.objects.filter(status = True).order_by('order_by') # getting contents of the respective Slider through the slug.
    except Sponsor.DoesNotExist:
        content = None
        
    data = {
        # 'tab_title': content[0].menu_id,
        'title': 'Sponsors',
        'content': content
    }
    
    return render(request, page('menu-content/gallery'), data)


def test_page(request):
    return render(request, page('menu-content/list'))
    
    



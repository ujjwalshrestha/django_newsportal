from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField, RichTextFormField
from ckeditor_uploader.fields import RichTextUploadingField # to activate upload option in ckeditor for uploading files to the server
from django.utils.safestring import mark_safe
from django.template.defaultfilters import truncatechars, truncatewords  # to slice words or characters from string in admin table
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.utils.text import slugify 
# Create your models here.

class Menu(models.Model):
    """This model is for creating dynamic menus.
    """
    
    is_main_menu_choices = (
        ('y', 'Yes'),
        ('n', 'No'),
    )
    is_main_menu = models.CharField(max_length=10, choices=is_main_menu_choices)
    menu_name = models.CharField(max_length = 100)
    slug = models.SlugField(max_length = 150, default = None, unique=True)
    parent_menu = models.ForeignKey('self', on_delete=models.CASCADE, limit_choices_to={'is_main_menu': 'y'}, blank=True, null=True)
    # sub_menu_name = models.CharField(max_length = 100, blank=True, null=True)
    page_type_choices = (
        ('dp', 'Detail Page'),
        ('slp', 'Simple Listing Page'),
        ('tlp', 'Thumbnail Listing Page'),
        ('clp', 'Collapsible Listing Page'),
        ('gp', 'Gallery Page'),
    )
    page_type = models.CharField(max_length=10, choices=page_type_choices)
    order_by = models.IntegerField()
    status = models.BooleanField(default=True, max_length=5)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    # updated_at = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return self.menu_name
    
    def clean(self):
        """
        Validate custom constraints. Here, this method determines that if the 'is_main_menu' = 'n', then the field 'parent_menu' is required.
        """
        # if self.is_main_menu == 'n' and self.parent_menu is None and self.sub_menu_name is None:
        if self.is_main_menu == 'n' and self.parent_menu is None:
            raise ValidationError({
                'parent_menu': _('This field is required.'),
                # 'sub_menu_name': _('This field is required.')
            })
            
    class Meta:
        verbose_name_plural = "Menu" # Renames the model to Menu
        
    def save(self, *args, **kwargs):
        """This allows changes in slug while editing.
        """
        self.slug = slugify(self.menu_name)
        return super(Menu, self).save(*args, **kwargs)
            
            
class MenuContent(models.Model):
    """This model is for maintaining contents of menu.
    """
    
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE)
    title = models.CharField(max_length = 255)
    slug = models.SlugField(max_length = 255, unique=True)
    description = RichTextUploadingField(blank=True, null=True)
    image = models.ImageField(max_length = 255, upload_to = 'images/menu/', blank=True, null=True, verbose_name="Thumbnail Image")
    files = models.FileField(max_length = 255, upload_to = 'files/menu/', blank=True, null=True)
    status = models.BooleanField(default=True, max_length=5)
    visited = models.IntegerField(default=0, blank=True, null=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, related_name='menucontent_created_by')
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    # updated_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, related_name='menucontent_updated_by')
    # updated_at = models.DateTimeField(auto_now=True)
   
    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name_plural = "Menu Contents" # Renames the model to Menu Contents
        
    def get_image(self):
        """This method displays the image thumbnail instead of image path in the list display of admin section.
        """
        return mark_safe("<img src='/media/%s' width=70>" % self.image)
    get_image.short_description = 'Images' # Renames the above declared Get Image to Images.
        
    def get_file(self):
        """This method displays 'Show File' instead of file path in the list display of admin section.
        """
        return mark_safe("<a href='/media/%s' target='_blank'>Show File</a>" % self.files)
    get_file.short_description = 'Files' # Renames the above declared Get File to Files.
    
    def get_description(self):
        """This method limits the words to 50 in the description field in the list.
        """
        return truncatewords(self.description, 50)
    get_description.short_description = 'Description' # Renames the above declared Get Description to Description.
    
    def save(self, *args, **kwargs):
        """This allows changes in slug while editing.
        """
        self.slug = slugify(self.title)
        return super(MenuContent, self).save(*args, **kwargs)
    
    
# @receiver(post_delete, sender = MenuContent) # NO required now
# def del_menucontent_img(sender, instance, **kwargs):
#     """ After the object/data is deleted, the image is unlinked fron the path through this function.
    
#     Arguments:
#         sender {string} -- Name of the model.
#         instance {string} -- [description]
#     """
#     instance.image.delete(False) 
    
    
class ImageSlider(models.Model):
    """This model is for maintaining contents of image slider.
    """
    
    image = models.ImageField(max_length=255, upload_to='images/slider')
    title = models.CharField(max_length=200)
    slug = models.SlugField(max_length = 255, unique=True)
    description = RichTextUploadingField(blank=True, null=True)
    order_by = models.IntegerField(default=0)
    status = models.BooleanField(default=True, max_length=5)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, related_name='imgslider_created_by')
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
 
    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name_plural = "Image Slider" # Renames the model to Menu Contents
        
    def get_image(self):
        """This method displays the image thumbnail instead of image path in the list display of admin section.
        """
        return mark_safe("<img src='/media/%s' width=70>" % self.image)
    get_image.short_description = 'Image' # Renames the above declared Get Image to Images.
    
    def get_description(self):
        """This method limits the words to 50 in the description field in the list.
        """
        return truncatewords(self.description, 25)
    get_description.short_description = 'Description' # Renames the above declared Get Description to Description
    
    def save(self, *args, **kwargs):
        """This allows changes in slug while editing.
        """
        self.slug = slugify(self.title)
        return super(ImageSlider, self).save(*args, **kwargs)
    

class QuickLinks(models.Model):
    """This model is for maintaining quicklinks as implemented on the footer of the site.
    """
    
    title = models.CharField(max_length=100)
    link = models.CharField(max_length=200)
    order_by = models.IntegerField()
    status = models.BooleanField(default=False)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, related_name='quicklinks_created_by')
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    
    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name_plural = "Quick Links" # Renames the model to Menu Contents
        
    def get_link(self):
        """This method displays 'Go to Links' instead of showing links directly in the list display of admin section.
        """
        return mark_safe("<a href='%s' target='_blank'>Go to Link</a>" % self.link)
    get_link.short_description = 'Links' # Renames the above declared Get File to Files.
    
    
class SocialLinks(models.Model):
    """This model is for maintaining quicklinks as implemented on the footer of the site.
    """
    
    social_site_choices = (
        ('facebook', 'Facebook'),
        ('twitter', 'Twitter'),
        ('flickr', 'Flickr'),
        ('pinterest', 'Pinterest'),
        ('googleplus', 'Google Plus'),
        ('vimeo', 'Vimeo'),
        ('youtube', 'Youtube'),
        ('gmail', 'Gmail'),
    )
    social_site = models.CharField(max_length=20, choices=social_site_choices, unique=True)
    link = models.CharField(max_length=200)
    order_by = models.IntegerField()
    status = models.BooleanField(default=False)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, related_name='socialinks_created_by')
    created_at = models.DateTimeField(auto_now_add=True, blank=True)
    
    def __str__(self):
        return self.social_site
    
    class Meta:
        verbose_name_plural = "Social Links" # Renames the model to Menu Contents
        
    def get_link(self):
        """This method displays 'Go to Links' instead of showing links directly in the list display of admin section.
        """
        return mark_safe("<a href='%s' target='_blank'>Go to Link</a>" % self.link)
    get_link.short_description = 'Links' # Renames the above declared Get File to Files.
    

class Horoscope(models.Model):
    """This model is for maintaining the horoscopre inital contents.
    """
    
    name = models.CharField(max_length=200)
    dates = models.CharField(max_length=200)
    image = models.ImageField(max_length = 255, upload_to = 'images/horoscope/', blank=True, null=True)
    order_by = models.IntegerField()
    status = models.BooleanField(default=False)
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = "Horoscope" # Renames the model to Menu Contents
        
    def get_image(self):
        """This method displays the image thumbnail instead of image path in the list display of admin section.
        """
        return mark_safe("<img src='/media/%s' width=70>" % self.image)
    get_image.short_description = 'Image' # Renames the above declared Get Image to Images.
    

class NewsArticleCategory(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length = 255, unique=True)
    status = models.BooleanField(default=False)
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = "News/Article Category" # Renames the model to News/Article Category
        
        
class NewsArticle(models.Model):
    select_type_choices = (
        ('n', 'News'),
        ('a', 'Article'),
    )
    select_type = models.CharField(max_length=10, choices=select_type_choices)
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length = 255, unique=True)
    status = models.BooleanField(default=False)
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = "News/Article Category" # Renames the model to Menu Contents
        
        
class SiteConfig(models.Model):
    select_config_choices = (
        ('wl', 'Website Logo'),
        ('wi', 'Website Icon'),
        ('wt', 'Website Title'),
        ('ab', 'Advertisement Banner'),
        ('ci', 'Contact Info'),
    )
    select_config = models.CharField(max_length=5, choices=select_config_choices)
    name = models.CharField(max_length=200, blank=True, null=True)
    description = RichTextUploadingField(blank=True, null=True)
    image = models.ImageField(max_length = 255, upload_to='images/site_config', blank=True, null=True)
    link = models.CharField(max_length=255, blank=True, null=True)
    status = models.BooleanField(default=False)
    
    def __str__(self):
        return self.select_config
    
    class Meta:
        verbose_name_plural = "Site Config" # Renames the model to Menu Contents
        
    def get_image(self):
        """This method displays the image thumbnail instead of image path in the list display of admin section.
        """
        return mark_safe("<img src='/media/%s' width=70>" % self.image)
    get_image.short_description = 'Image' # Renames the above declared Get Image to Images.
    
    def get_description(self):
        """This method limits the words to 50 in the description field in the list.
        """
        return truncatewords(self.description, 50)
    get_description.short_description = 'Description' # Renames the above declared Get Description to Description
    
    
class Faq(models.Model):
    question = models.CharField(max_length = 255)
    answer = RichTextUploadingField()
    order_by = models.IntegerField(default=0)
    status = models.BooleanField(default = True)
    created_by = models.ForeignKey(User, on_delete = models.CASCADE, blank=True, related_name='faq_created_by')
    created_at = models.DateTimeField(auto_now_add = True, blank = True)
    
    def __str__(self):
        return self.question
    
    class Meta:
        verbose_name_plural = "FAQ" # Renames the model to FAQ
    
    def get_answer(self):
        """This method limits the words to 50 in the answer field in the list.
        """
        return truncatewords(self.answer, 50)
    get_answer.short_description = 'Answer' # Renames the above declared Get Description to Description
    

class Sponsor(models.Model):
    title = models.CharField(max_length = 255)
    image = models.ImageField(max_length = 255, upload_to = 'images/sponsor')
    link = models.CharField(max_length = 255, blank = True, null = True)
    order_by = models.IntegerField(default = 0)
    status = models.BooleanField(default = True)
    created_by = models.ForeignKey(User, on_delete = models.CASCADE, blank = True, related_name = 'sponsor_created_by')
    created_at = models.DateTimeField(auto_now_add = True, blank = True)
    
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = "Sponsor" # Renames the model to Sponsor
        
    def get_image(self):
        """This method displays the image thumbnail instead of image path in the list display of admin section.
        """
        return mark_safe("<img src='/media/%s' width=70>" % self.image)
    get_image.short_description = 'Image' # Renames the above declared Get Image to Images.
  
    
class Popup(models.Model):
    image = models.ImageField(max_length = 255, upload_to = 'images/popup')
    link = models.CharField(max_length = 255, blank = True, null = True)
    link_name = models.CharField(max_length = 255, blank = True, null = True)
    order_by = models.IntegerField(default = 0)
    status = models.BooleanField(default = True)
    created_by = models.ForeignKey(User, on_delete = models.CASCADE, blank = True, related_name = 'popup_created_by')
    created_at = models.DateTimeField(auto_now_add = True, blank = True)
    
    
    class Meta:
        verbose_name_plural = "Popup" # Renames the model to Sponsor
        
    def get_image(self):
        """This method displays the image thumbnail instead of image path in the list display of admin section.
        """
        return mark_safe("<img src='/media/%s' width=70>" % self.image)
    get_image.short_description = 'Image' # Renames the above declared Get Image to Images.
    
    
    
    
        

    
from django.contrib import admin
from .models import *
# from .models import (Menu, MenuContent, ImageSlider, QuickLinks, SocialLinks, Horoscope, NewsArticleCategory, SiteConfig, Faq, Sponsor, Popup)
# Register your models here.

@admin.register(Menu)
class MenuAdmin(admin.ModelAdmin):
    """This ModelAdmin is registered for model Menu.
    """
    
    list_display = ['menu_name', 'slug', 'parent_menu', 'page_type', 'order_by', 'status', 'created_by']
    fieldsets = [(None, 
        { 
            'fields': ('is_main_menu', 'menu_name', 'slug', 'parent_menu', 'page_type', 'order_by', 'status') 
        }),
    ] # fieldsets: fields to be shown while create and update in admin section
    search_fields = ['menu_name', 'slug', 'parent_menu__menu_name', 'page_type', 'created_by__username'] # This denotes the name of the fields through which contents can be searched.
    list_filter = ['menu_name', 'created_at', 'status'] # This denotes the name of the fields through which contents can be filtered.
    list_per_page = 10 #pagination
    prepopulated_fields = {'slug': ('menu_name',)}
    actions = ['update_status_active', 'update_status_inactive'] # Displayed in actions dropdown. Methods name as deinfed below is called in this section. 
    
    def update_status_active(self, request, queryset):
        """ To activate the status of any content of News module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to true.
        """
        queryset.update(status = True)
        
    update_status_active.short_description = 'Activate Selected Menu'
        
    def update_status_inactive(self, request, queryset):
        """To deactivate the status of any content of News module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to false.
        """
        queryset.update(status = False)
        
    update_status_inactive.short_description = 'Deactivate Selected Menu'
    
    def save_model(self, request, obj, form, change):
        """Through this method, the userid of logged in user in the admin section is inserted in the 'created_by' field.
        """
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()
        

@admin.register(MenuContent)
class MenuContentAdmin(admin.ModelAdmin):
    """This ModelAdmin is registered for model MenuContent.
    """
    
    list_display = ['menu', 'title', 'get_description', 'get_image', 'get_file', 'status', 'created_by']
    fieldsets = [(None, 
        { 
            'fields': ('menu', 'title', 'slug', 'description', 'image', 'files', 'status') 
        }),
    ] # fieldsets: fields to be shown while create and update in admin section
    search_fields = ['menu__menu_name', 'title', 'created_by__username'] # This denotes the name of the fields through which contents can be searched.
    list_filter = ['title', 'created_at', 'status'] # This denotes the name of the fields through which contents can be filtered.
    list_per_page = 10 #pagination
    list_display_links = ['title']
    prepopulated_fields = {'slug': ('title',)}
    actions = ['update_status_active', 'update_status_inactive'] # Displayed in actions dropdown. Methods name as deinfed below is called in this section. 
    
    def update_status_active(self, request, queryset):
        """ To activate the status of any content of News module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to true.
        """
        queryset.update(status = True)
        
    update_status_active.short_description = 'Activate Selected Menu Contents'
        
    def update_status_inactive(self, request, queryset):
        """To deactivate the status of any content of News module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to false.
        """
        queryset.update(status = False)
        
    update_status_inactive.short_description = 'Deactivate Selected Menu Contents'
    
    def save_model(self, request, obj, form, change):
        """Through this method, the userid of logged in user in the admin section is inserted in the 'created_by' field.
        """
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()
        

@admin.register(ImageSlider)     
class ImageSliderAdmin(admin.ModelAdmin):
    """This ModelAdmin is registered for model ImageSlider.
    """
    
    list_display = ['get_image', 'title', 'get_description', 'order_by', 'status', 'created_by']
    fieldsets = [(None, 
        { 
            'fields': ('image', 'title', 'slug', 'description', 'order_by', 'status') 
        }),
    ] # fieldsets: fields to be shown while create and update in admin section
    search_fields = ['title', 'created_by__username'] # This denotes the name of the fields through which contents can be searched.
    list_filter = ['title', 'created_at', 'status'] # This denotes the name of the fields through which contents can be filtered.
    list_per_page = 10 #pagination
    list_display_links = ['title']
    prepopulated_fields = {'slug': ('title',)}
    actions = ['update_status_active', 'update_status_inactive'] # Displayed in actions dropdown. Methods name as deinfed below is called in this section. 
    
    def update_status_active(self, request, queryset):
        """ To activate the status of any content of News module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to true.
        """
        queryset.update(status = True)
        
    update_status_active.short_description = 'Activate Selected Slider Images'
        
    def update_status_inactive(self, request, queryset):
        """To deactivate the status of any content of News module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to false.
        """
        queryset.update(status = False)
        
    update_status_inactive.short_description = 'Deactivate Selected Slider Images'
    
    def save_model(self, request, obj, form, change):
        """Through this method, the userid of logged in user in the admin section is inserted in the 'created_by' field.
        """
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()
        

@admin.register(QuickLinks)
class QuickLinksAdmin(admin.ModelAdmin):
    """This ModelAdmin is registered for model QuickLinks.
    """
    list_display = ['title', 'get_link', 'order_by', 'status', 'created_by']
    fieldsets = [(None, 
        { 
            'fields': ('title', 'link', 'order_by', 'status') 
        }),
    ] # fieldsets: fields to be shown while create and update in admin section
    search_fields = ['title', 'created_by__username'] # This denotes the name of the fields through which contents can be searched.
    list_filter = ['title', 'created_at', 'status'] # This denotes the name of the fields through which contents can be filtered.
    list_per_page = 10 #pagination
    actions = ['update_status_active', 'update_status_inactive'] # Displayed in actions dropdown. Methods name as deinfed below is called in this section. 
    
    def update_status_active(self, request, queryset):
        """ To activate the status of any content of News module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to true.
        """
        queryset.update(status = True)
        
    update_status_active.short_description = 'Activate Selected Quick Links'
        
    def update_status_inactive(self, request, queryset):
        """To deactivate the status of any content of News module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to false.
        """
        queryset.update(status = False)
        
    update_status_inactive.short_description = 'Deactivate Selected Quick Links'
    
    def save_model(self, request, obj, form, change):
        """Through this method, the userid of logged in user in the admin section is inserted in the 'created_by' field.
        """
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()
        
        
@admin.register(SocialLinks)
class SocialLinksAdmin(admin.ModelAdmin):
    """This ModelAdmin is registered for model SocialLinks.
    """
    list_display = ['social_site', 'get_link', 'order_by', 'status', 'created_by']
    fieldsets = [(None, 
        { 
            'fields': ('social_site', 'link', 'order_by', 'status') 
        }),
    ] # fieldsets: fields to be shown while create and update in admin section
    search_fields = ['social_site', 'created_by__username'] # This denotes the name of the fields through which contents can be searched.
    list_filter = ['social_site', 'created_at', 'status'] # This denotes the name of the fields through which contents can be filtered.
    list_per_page = 10 #pagination
    actions = ['update_status_active', 'update_status_inactive'] # Displayed in actions dropdown. Methods name as deinfed below is called in this section. 
    
    def update_status_active(self, request, queryset):
        """ To activate the status of any content of News module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to true.
        """
        queryset.update(status = True)
        
    update_status_active.short_description = 'Activate Selected Social Links'
        
    def update_status_inactive(self, request, queryset):
        """To deactivate the status of any content of News module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to false.
        """
        queryset.update(status = False)
        
    update_status_inactive.short_description = 'Deactivate Selected Social Links'
    
    def save_model(self, request, obj, form, change):
        """Through this method, the userid of logged in user in the admin section is inserted in the 'created_by' field.
        """
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()
    
        
    
@admin.register(Horoscope)
class HoroscopeAdmin(admin.ModelAdmin):
    """This ModelAdmin is registered for model Horoscope.
    """
    list_display = ['name', 'dates', 'get_image', 'order_by', 'status']
    fieldsets = [(None, 
        { 
            'fields': ('name', 'dates', 'image', 'order_by', 'status') 
        }),
    ] # fieldsets: fields to be shown while create and update in admin section
    search_fields = ['name'] # This denotes the name of the fields through which contents can be searched.
    list_filter = ['name', 'status'] # This denotes the name of the fields through which contents can be filtered.
    list_per_page = 10 #pagination
    ordering = ['order_by']
    actions = ['update_status_active', 'update_status_inactive'] # Displayed in actions dropdown. Methods name as deinfed below is called in this section. 
    
    def update_status_active(self, request, queryset):
        """ To activate the status of any content of News module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to true.
        """
        queryset.update(status = True)
        
    update_status_active.short_description = 'Activate Selected Horoscope'
        
    def update_status_inactive(self, request, queryset):
        """To deactivate the status of any content of News module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to false.
        """
        queryset.update(status = False)
        
    update_status_inactive.short_description = 'Deactivate Selected Horoscope'
    

@admin.register(NewsArticleCategory)
class NewsArticleCategoryAdmin(admin.ModelAdmin):
    """This ModelAdmin is registered for model NewsArticleCategory.
    """
    list_display = ['name', 'status']
    fieldsets = [(None, 
        { 
            'fields': ('name', 'slug', 'status') 
        }),
    ] # fieldsets: fields to be shown while create and update in admin section
    search_fields = ['name'] # This denotes the name of the fields through which contents can be searched.
    list_filter = ['name', 'status'] # This denotes the name of the fields through which contents can be filtered.
    list_per_page = 10 #pagination
    prepopulated_fields = {'slug': ('name',)} # slug will be generating with respect to title.
    actions = ['update_status_active', 'update_status_inactive'] # Displayed in actions dropdown. Methods name as deinfed below is called in this section. 
    
    def update_status_active(self, request, queryset):
        """ To activate the status of any content of News module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to true.
        """
        queryset.update(status = True)
        
    update_status_active.short_description = 'Activate Selected Categories'
        
    def update_status_inactive(self, request, queryset):
        """To deactivate the status of any content of News module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to false.
        """
        queryset.update(status = False)
        
    update_status_inactive.short_description = 'Deactivate Selected Categories'
    

@admin.register(SiteConfig)
class SiteConfigAdmin(admin.ModelAdmin):
    """This ModelAdmin is registered for model SiteConfig.
    """
    list_display = ['select_config', 'status']
    search_fields = ['select_config', 'name'] # This denotes the name of the fields through which contents can be searched.
    list_filter = ['select_config', 'name', 'status'] # This denotes the name of the fields through which contents can be filtered.
    list_per_page = 10 #pagination
    actions = ['update_status_active', 'update_status_inactive'] # Displayed in actions dropdown. Methods name as deinfed below is called in this section. 
    
    def update_status_active(self, request, queryset):
        """ To activate the status of any content of News module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to true.
        """
        queryset.update(status = True)
        
    update_status_active.short_description = 'Activate Selected Site Config'
        
    def update_status_inactive(self, request, queryset):
        """To deactivate the status of any content of News module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to false.
        """
        queryset.update(status = False)
        
    update_status_inactive.short_description = 'Deactivate Selected Site Config'
    
    
@admin.register(Faq)
class FaqAdmin(admin.ModelAdmin):
    """This ModelAdmin is registered for model Faq.
    """
    list_display = ['question', 'get_answer', 'order_by', 'status']
    fieldsets = [(None, 
        { 
            'fields': ('question', 'answer', 'order_by', 'status') 
        }),
    ] # fieldsets: fields to be shown while create and update in admin section
    search_fields = ['question', 'answer'] # This denotes the name of the fields through which contents can be searched.
    list_filter = ['question', 'created_at', 'status'] # This denotes the name of the fields through which contents can be filtered.
    list_per_page = 10 #pagination
    actions = ['update_status_active', 'update_status_inactive'] # Displayed in actions dropdown. Methods name as deinfed below is called in this section. 
    
    def update_status_active(self, request, queryset):
        """ To activate the status of any content of FAQ module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to true.
        """
        queryset.update(status = True)
        
    update_status_active.short_description = 'Activate Selected FAQ'
        
    def update_status_inactive(self, request, queryset):
        """To deactivate the status of any content of FAQ module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to false.
        """
        queryset.update(status = False)
        
    update_status_inactive.short_description = 'Deactivate Selected FAQ'
    
    def save_model(self, request, obj, form, change):
        """Through this method, the userid of logged in user in the admin section is inserted in the 'created_by' field.
        """
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()
        
        
@admin.register(Sponsor)
class SponsorAdmin(admin.ModelAdmin):
    """This ModelAdmin is registered for model Faq.
    """
    list_display = ['title', 'get_image', 'order_by', 'status']
    fieldsets = [(None, 
        { 
            'fields': ('title', 'image', 'link', 'order_by', 'status') 
        }),
    ] # fieldsets: fields to be shown while create and update in admin section
    search_fields = ['title'] # This denotes the name of the fields through which contents can be searched.
    list_filter = ['title', 'created_at', 'status'] # This denotes the name of the fields through which contents can be filtered.
    list_per_page = 10 #pagination
    actions = ['update_status_active', 'update_status_inactive'] # Displayed in actions dropdown. Methods name as deinfed below is called in this section. 
    
    def update_status_active(self, request, queryset):
        """ To activate the status of any content of FAQ module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to true.
        """
        queryset.update(status = True)
        
    update_status_active.short_description = 'Activate Selected Sponsors'
        
    def update_status_inactive(self, request, queryset):
        """To deactivate the status of any content of FAQ module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to false.
        """
        queryset.update(status = False)
        
    update_status_inactive.short_description = 'Deactivate Selected Sponsors'
    
    def save_model(self, request, obj, form, change):
        """Through this method, the userid of logged in user in the admin section is inserted in the 'created_by' field.
        """
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()
        
@admin.register(Popup)       
class PopupAdmin(admin.ModelAdmin):
    """This ModelAdmin is registered for model Faq.
    """
    list_display = ['get_image', 'link', 'link_name', 'order_by', 'status']
    fieldsets = [(None, 
        { 
            'fields': ('image', 'link', 'link_name', 'order_by', 'status') 
        }),
    ] # fieldsets: fields to be shown while create and update in admin section
    search_fields = ['title', 'created_at', 'status'] # This denotes the name of the fields through which contents can be searched.
    list_per_page = 10 #pagination
    actions = ['update_status_active', 'update_status_inactive'] # Displayed in actions dropdown. Methods name as deinfed below is called in this section. 
    
    def update_status_active(self, request, queryset):
        """ To activate the status of any content of FAQ module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to true.
        """
        queryset.update(status = True)
        
    update_status_active.short_description = 'Activate Selected Popup'
        
    def update_status_inactive(self, request, queryset):
        """To deactivate the status of any content of FAQ module through actions.
        
        Arguments:
            request {Request} -- Required.
            queryset {Query} -- Builtin query through which status is updated to false.
        """
        queryset.update(status = False)
        
    update_status_inactive.short_description = 'Deactivate Selected Popup'
    
    def save_model(self, request, obj, form, change):
        """Through this method, the userid of logged in user in the admin section is inserted in the 'created_by' field.
        """
        if getattr(obj, 'created_by', None) is None:
            obj.created_by = request.user
        obj.save()
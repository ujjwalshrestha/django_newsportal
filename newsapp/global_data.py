from .models import *
from django.db.models import Q
from django.http import *

def get_menu(request):
    """Through this function, all the menu are displayed dynamically.
    
    Arguments:
        request {Request} -- Request
    """
    data = {
        'menu_data': Menu.objects.prefetch_related('menu_set').filter(status = True).order_by('order_by')
    }
    
    return data


def get_links(request):
    """Through this function, all the quick links and social links are displayed dynamically.
    
    Arguments:
        request {Request} -- Request
    """
    data = {
        'quicklinks_data': QuickLinks.objects.filter(status = True).order_by('order_by'),
        'sociallinks_data': SocialLinks.objects.filter(status = True).order_by('order_by')
    }
    
    return data



def get_siteconfig(request):
    """Through this function, all the site config data are displayed dynamically.
    
    Arguments:
        request {Request} -- Request
    """
    data = {
        'site_logo': SiteConfig.objects.filter(status = True, select_config = 'wl'),
        'site_icon': SiteConfig.objects.filter(status = True, select_config = 'wi'),
        'site_title': SiteConfig.objects.filter(status = True, select_config = 'wt'),
        'contact_info': SiteConfig.objects.filter(status = True, select_config = 'ci'),
        'ad_banner': SiteConfig.objects.filter(status = True, select_config = 'ab')
    }
    
    return data


def get_globalcontent(request):
    """Through this function, all the global contents required are displayed dynamically.
    
    Arguments:
        request {Request} -- Request
    """
    data = {
        'latest_news': MenuContent.objects.filter(status = True, menu_id = 7).order_by('-created_at')[:5],
        'latest_post': MenuContent.objects.filter(status = True, menu__status = True).exclude(Q(menu__page_type='dp') | Q(menu__page_type='gp')).order_by('-created_at')[:5],
        'popular_post': MenuContent.objects.filter(status = True, menu__status = True).exclude(Q(menu__page_type='dp') | Q(menu__page_type='gp')).order_by('-visited')[:5],
        'about_us': MenuContent.objects.filter(status = True, id = 1),
    }
    
    return data
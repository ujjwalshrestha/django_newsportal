from django.urls import path, include, re_path
from .views import *

urlpatterns= [
    path('', index, name='home'),
    path('contact', contact, name='contact'),
    path('menu-content/<slug>', menu_content, name='menu-content'),
    path('test-page', test_page, name='test-page'),
    path('details/<slug>', details, name='details'),
    path('horoscope-detail/<param>', horoscope_detail, name='horoscope-detail'),
    path('slider-detail/<slug>', slider_detail, name='slider-detail'),
    path('sponsors', sponsors, name='sponsors'),
]
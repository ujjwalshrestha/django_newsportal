from django import forms
from .models import *
from crispy_forms.helper import FormHelper
from django.utils.safestring import mark_safe
from helper import *
from django.core.validators import validate_email #import used for email validation in forms

class ContactForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Name*'}), label='', required=False,)
    
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Email*'}), label='', required=False,)
    
    subject = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Subject*'}), label='', required=False,)
    
    message = forms.CharField(widget=forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Message*', 'cols': '30', 'rows': '10'}), label='', required=False,)
    
    def clean_name(self):
        name = self.cleaned_data['name']
        if name == '':
            raise forms.ValidationError("This field is required.")
        else:
            return name
    
    def clean_email(self):
        email = self.cleaned_data['email']
        if email == '':
            raise forms.ValidationError("This field is required.")
        else:
            try:
                mt = validate_email(email)
            except:
                raise forms.ValidationError("Email format incorrect.")
            return email
        
    def clean_subject(self):
        subject = self.cleaned_data['subject']
        if subject == '':
            raise forms.ValidationError("This field is required.")
        else:
            return subject
    
    def clean_message(self):
        message = self.cleaned_data['message']
        if message == '':
            raise forms.ValidationError("This field is required.")
        else:
            return message
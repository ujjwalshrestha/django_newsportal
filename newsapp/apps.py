from django.apps import AppConfig
from suit.apps import DjangoSuitConfig #V2 django-suit
from suit.menu import ParentItem, ChildItem #V2 django-suit

class NewsappConfig(AppConfig):
    name = 'newsapp'
    
class SuitConfig(DjangoSuitConfig): #V2 django-suit
    # layout = "vertical"
    
    menu = (
        ParentItem('Authentication', children=[
            ChildItem(model='auth.user'),
            ChildItem('User groups', 'auth.group'),
        ], icon='fa fa-users'),
        ParentItem('Content', children=[
            ChildItem(model='newsapp.menu'),
            ChildItem(model='newsapp.menucontent'),
            ChildItem(model='newsapp.imageslider'),
            ChildItem(model='newsapp.quicklinks'),
            ChildItem(model='newsapp.sociallinks'),
            ChildItem(model='newsapp.horoscope'),
            ChildItem(model='newsapp.siteconfig'),
            ChildItem(model='newsapp.faq'),
            ChildItem(model='newsapp.sponsor'),
            ChildItem(model='newsapp.popup'),
            # ChildItem('Custom view', url='/admin/custom/'),
        ], icon='fa fa-leaf')
        # ParentItem('Right Side Menu', children=[
        #     ChildItem('Password change', url='admin:password_change'),
        #     ChildItem('Open Google', url='http://google.com', target_blank=True),

        # ], align_right=True, icon='fa fa-cog'),
    )

    def ready(self):
        super(SuitConfig, self).ready()

        # DO NOT COPY FOLLOWING LINE
        # It is only to prevent updating last_login in DB for demo app
        self.prevent_user_last_login()

    def prevent_user_last_login(self):
        """
        Disconnect last login signal
        """
        from django.contrib.auth import user_logged_in
        from django.contrib.auth.models import update_last_login
        user_logged_in.disconnect(update_last_login)

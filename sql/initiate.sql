-- Select rows from a Table or View 'TableOrViewName' in schema 'SchemaName'
SELECT * 
FROM newsapp_menu AS pm
LEFT JOIN newsapp_menu AS cm
ON pm.id = cm.parent_menu_id 
WHERE pm.status = 1 OR cm.status = 1;
-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 26, 2019 at 08:02 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `newsportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add user', 4, 'add_user'),
(14, 'Can change user', 4, 'change_user'),
(15, 'Can delete user', 4, 'delete_user'),
(16, 'Can view user', 4, 'view_user'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session'),
(25, 'Can add menu', 7, 'add_menu'),
(26, 'Can change menu', 7, 'change_menu'),
(27, 'Can delete menu', 7, 'delete_menu'),
(28, 'Can view menu', 7, 'view_menu'),
(29, 'Can add menu content', 8, 'add_menucontent'),
(30, 'Can change menu content', 8, 'change_menucontent'),
(31, 'Can delete menu content', 8, 'delete_menucontent'),
(32, 'Can view menu content', 8, 'view_menucontent'),
(33, 'Can add image slider', 9, 'add_imageslider'),
(34, 'Can change image slider', 9, 'change_imageslider'),
(35, 'Can delete image slider', 9, 'delete_imageslider'),
(36, 'Can view image slider', 9, 'view_imageslider'),
(37, 'Can add quick links', 10, 'add_quicklinks'),
(38, 'Can change quick links', 10, 'change_quicklinks'),
(39, 'Can delete quick links', 10, 'delete_quicklinks'),
(40, 'Can view quick links', 10, 'view_quicklinks'),
(41, 'Can add social links', 11, 'add_sociallinks'),
(42, 'Can change social links', 11, 'change_sociallinks'),
(43, 'Can delete social links', 11, 'delete_sociallinks'),
(44, 'Can view social links', 11, 'view_sociallinks'),
(45, 'Can add horoscope', 12, 'add_horoscope'),
(46, 'Can change horoscope', 12, 'change_horoscope'),
(47, 'Can delete horoscope', 12, 'delete_horoscope'),
(48, 'Can view horoscope', 12, 'view_horoscope'),
(49, 'Can add news article category', 13, 'add_newsarticlecategory'),
(50, 'Can change news article category', 13, 'change_newsarticlecategory'),
(51, 'Can delete news article category', 13, 'delete_newsarticlecategory'),
(52, 'Can view news article category', 13, 'view_newsarticlecategory'),
(53, 'Can add news article', 14, 'add_newsarticle'),
(54, 'Can change news article', 14, 'change_newsarticle'),
(55, 'Can delete news article', 14, 'delete_newsarticle'),
(56, 'Can view news article', 14, 'view_newsarticle'),
(57, 'Can add site config', 15, 'add_siteconfig'),
(58, 'Can change site config', 15, 'change_siteconfig'),
(59, 'Can delete site config', 15, 'delete_siteconfig'),
(60, 'Can view site config', 15, 'view_siteconfig'),
(61, 'Can add faq', 16, 'add_faq'),
(62, 'Can change faq', 16, 'change_faq'),
(63, 'Can delete faq', 16, 'delete_faq'),
(64, 'Can view faq', 16, 'view_faq'),
(65, 'Can add sponsor', 17, 'add_sponsor'),
(66, 'Can change sponsor', 17, 'change_sponsor'),
(67, 'Can delete sponsor', 17, 'delete_sponsor'),
(68, 'Can view sponsor', 17, 'view_sponsor'),
(69, 'Can add popup', 18, 'add_popup'),
(70, 'Can change popup', 18, 'change_popup'),
(71, 'Can delete popup', 18, 'delete_popup'),
(72, 'Can view popup', 18, 'view_popup');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$150000$mSSV27PvgVXT$0INfEPBYF8EbMENxy9JjcYQqq1Z4ymL8+3MEBuiToV8=', '2019-09-25 14:08:10.426743', 1, 'admin', '', '', 'admin@gmail.com', 1, 1, '2019-08-06 06:43:25.341845');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext DEFAULT NULL,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_admin_log`
--

INSERT INTO `django_admin_log` (`id`, `action_time`, `object_id`, `object_repr`, `action_flag`, `change_message`, `content_type_id`, `user_id`) VALUES
(1, '2019-08-06 07:16:31.880528', '1', 'About', 1, '[{\"added\": {}}]', 7, 1),
(2, '2019-08-06 07:17:14.612903', '2', 'Articles', 1, '[{\"added\": {}}]', 7, 1),
(3, '2019-08-06 07:17:28.109495', '3', 'Journal Paper', 1, '[{\"added\": {}}]', 7, 1),
(4, '2019-08-06 07:17:39.291443', '3', 'Journal Paper', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 7, 1),
(5, '2019-08-06 07:17:48.679117', '3', 'Journal Paper', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 7, 1),
(6, '2019-08-06 07:17:52.077530', '2', 'Articles', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 7, 1),
(7, '2019-08-06 07:27:01.646401', '2', 'Articles', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 7, 1),
(8, '2019-08-06 07:27:11.746090', '3', 'Journal Paper', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 7, 1),
(9, '2019-08-06 07:41:57.062405', '3', 'Journal Paper', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 7, 1),
(10, '2019-08-06 08:08:33.966658', '4', 'Blog', 1, '[{\"added\": {}}]', 7, 1),
(11, '2019-08-06 08:15:15.727808', '3', 'Journal Paper', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 7, 1),
(12, '2019-08-06 08:15:23.404262', '4', 'Blog', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 7, 1),
(13, '2019-08-06 08:20:23.172783', '4', 'Blog', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 7, 1),
(14, '2019-08-06 08:20:31.244520', '3', 'Journal Paper', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 7, 1),
(15, '2019-08-06 08:26:12.675265', '4', 'Blog', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 7, 1),
(16, '2019-08-06 08:26:16.344854', '3', 'Journal Paper', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 7, 1),
(17, '2019-08-09 11:45:12.972560', '1', 'About Us', 1, '[{\"added\": {}}]', 8, 1),
(18, '2019-08-09 12:05:37.440001', '1', 'About Us', 1, '[{\"added\": {}}]', 8, 1),
(19, '2019-08-09 15:13:35.902870', '3', 'Journal Paper', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 7, 1),
(20, '2019-08-09 15:13:40.007807', '3', 'Journal Paper', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(21, '2019-08-09 15:15:09.279239', '2', 'This is a test title', 1, '[{\"added\": {}}]', 8, 1),
(22, '2019-08-09 15:35:40.476636', '3', 'This is a test title 2', 1, '[{\"added\": {}}]', 8, 1),
(23, '2019-08-09 15:36:21.794023', '3', 'This is a test title 2', 2, '[{\"changed\": {\"fields\": [\"image\", \"files\"]}}]', 8, 1),
(24, '2019-08-10 22:47:33.938461', '3', 'This is a test title 2', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 8, 1),
(25, '2019-08-10 22:47:57.803424', '3', 'This is a test title 2', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 8, 1),
(26, '2019-08-11 10:51:43.380233', '1', 'BBC News', 1, '[{\"added\": {}}]', 10, 1),
(27, '2019-08-11 11:35:49.207677', '1', 'facebook', 1, '[{\"added\": {}}]', 11, 1),
(28, '2019-08-11 12:44:28.860300', '1', 'facebook', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 11, 1),
(29, '2019-08-11 12:45:38.413912', '2', 'twitter', 1, '[{\"added\": {}}]', 11, 1),
(30, '2019-08-11 12:45:47.656468', '1', 'facebook', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 11, 1),
(31, '2019-08-11 12:58:33.227760', '2', 'CNN', 1, '[{\"added\": {}}]', 10, 1),
(32, '2019-08-11 12:58:37.618949', '1', 'BBC', 2, '[{\"changed\": {\"fields\": [\"title\"]}}]', 10, 1),
(33, '2019-08-11 12:59:03.041157', '3', 'Yahoo News', 1, '[{\"added\": {}}]', 10, 1),
(34, '2019-08-11 12:59:24.581055', '4', 'Google News', 1, '[{\"added\": {}}]', 10, 1),
(35, '2019-08-11 13:01:18.430593', '5', 'ABC News', 1, '[{\"added\": {}}]', 10, 1),
(36, '2019-08-11 13:01:48.212316', '6', 'DailyMail', 1, '[{\"added\": {}}]', 10, 1),
(37, '2019-08-11 13:02:04.959501', '7', 'Fox News', 1, '[{\"added\": {}}]', 10, 1),
(38, '2019-08-11 13:03:15.809220', '3', 'youtube', 1, '[{\"added\": {}}]', 11, 1),
(39, '2019-08-11 13:03:29.635048', '4', 'gmail', 1, '[{\"added\": {}}]', 11, 1),
(40, '2019-08-11 13:18:21.020907', '5', 'Daily Horoscope', 1, '[{\"added\": {}}]', 7, 1),
(41, '2019-08-11 15:04:43.251237', '1', 'Aries', 1, '[{\"added\": {}}]', 12, 1),
(42, '2019-08-11 15:05:00.955880', '2', 'Taurus', 1, '[{\"added\": {}}]', 12, 1),
(43, '2019-08-11 15:05:13.778559', '3', 'Gemini', 1, '[{\"added\": {}}]', 12, 1),
(44, '2019-08-11 15:05:24.054568', '4', 'Cancer', 1, '[{\"added\": {}}]', 12, 1),
(45, '2019-08-11 15:07:20.611965', '5', 'Leo', 1, '[{\"added\": {}}]', 12, 1),
(46, '2019-08-11 15:07:47.666210', '6', 'Virgo', 1, '[{\"added\": {}}]', 12, 1),
(47, '2019-08-11 15:08:05.591607', '7', 'Libra', 1, '[{\"added\": {}}]', 12, 1),
(48, '2019-08-11 15:08:17.119033', '8', 'Scorpio', 1, '[{\"added\": {}}]', 12, 1),
(49, '2019-08-11 15:08:53.728823', '9', 'Sagittarius', 1, '[{\"added\": {}}]', 12, 1),
(50, '2019-08-11 15:09:08.053011', '10', 'Capricorn', 1, '[{\"added\": {}}]', 12, 1),
(51, '2019-08-11 15:09:16.455039', '11', 'Aquarius', 1, '[{\"added\": {}}]', 12, 1),
(52, '2019-08-11 15:09:43.729505', '12', 'Pisces', 1, '[{\"added\": {}}]', 12, 1),
(53, '2019-08-11 23:05:44.286953', '1', 'Sports', 1, '[{\"added\": {}}]', 13, 1),
(54, '2019-08-19 22:50:35.378405', '4', 'Blog', 3, '', 7, 1),
(55, '2019-08-19 22:50:35.434425', '3', 'Journal Paper', 3, '', 7, 1),
(56, '2019-08-19 22:50:35.479458', '2', 'Articles', 3, '', 7, 1),
(57, '2019-08-19 22:51:10.135089', '6', 'Contact', 1, '[{\"added\": {}}]', 7, 1),
(58, '2019-08-19 23:42:41.564683', '1', 'wl', 1, '[{\"added\": {}}]', 15, 1),
(59, '2019-08-19 23:43:03.520288', '2', 'wi', 1, '[{\"added\": {}}]', 15, 1),
(60, '2019-08-19 23:44:00.632670', '3', 'wt', 1, '[{\"added\": {}}]', 15, 1),
(61, '2019-08-19 23:46:03.366485', '4', 'ci', 1, '[{\"added\": {}}]', 15, 1),
(62, '2019-08-20 00:51:09.342009', '1', 'wl', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 15, 1),
(63, '2019-08-20 01:09:52.661482', '4', 'ci', 2, '[{\"changed\": {\"fields\": [\"description\"]}}]', 15, 1),
(64, '2019-08-20 01:11:06.306827', '4', 'ci', 2, '[{\"changed\": {\"fields\": [\"description\"]}}]', 15, 1),
(65, '2019-08-20 01:11:41.749018', '4', 'ci', 2, '[{\"changed\": {\"fields\": [\"description\"]}}]', 15, 1),
(66, '2019-08-20 01:12:17.156186', '4', 'ci', 2, '[{\"changed\": {\"fields\": [\"description\"]}}]', 15, 1),
(67, '2019-08-20 01:14:35.024949', '3', 'wt', 2, '[{\"changed\": {\"fields\": [\"name\"]}}]', 15, 1),
(68, '2019-08-20 01:14:48.828761', '3', 'wt', 2, '[{\"changed\": {\"fields\": [\"name\"]}}]', 15, 1),
(69, '2019-08-20 10:08:56.191727', '5', 'ab', 1, '[{\"added\": {}}]', 15, 1),
(70, '2019-08-20 14:01:24.302294', '4', 'ci', 2, '[{\"changed\": {\"fields\": [\"description\"]}}]', 15, 1),
(71, '2019-08-20 14:01:43.113666', '4', 'ci', 2, '[{\"changed\": {\"fields\": [\"description\"]}}]', 15, 1),
(72, '2019-08-20 16:06:43.368181', '1', 'Aries', 2, '[{\"changed\": {\"fields\": [\"dates\"]}}]', 12, 1),
(73, '2019-08-20 16:06:55.581478', '2', 'Taurus', 2, '[{\"changed\": {\"fields\": [\"dates\"]}}]', 12, 1),
(74, '2019-08-20 16:07:07.788834', '3', 'Gemini', 2, '[{\"changed\": {\"fields\": [\"dates\"]}}]', 12, 1),
(75, '2019-08-20 16:07:19.365788', '4', 'Cancer', 2, '[{\"changed\": {\"fields\": [\"dates\"]}}]', 12, 1),
(76, '2019-08-20 16:07:32.637220', '5', 'Leo', 2, '[{\"changed\": {\"fields\": [\"dates\"]}}]', 12, 1),
(77, '2019-08-20 16:07:45.580523', '6', 'Virgo', 2, '[{\"changed\": {\"fields\": [\"dates\"]}}]', 12, 1),
(78, '2019-08-20 16:07:57.516084', '7', 'Libra', 2, '[{\"changed\": {\"fields\": [\"dates\"]}}]', 12, 1),
(79, '2019-08-20 16:08:11.493125', '8', 'Scorpio', 2, '[{\"changed\": {\"fields\": [\"dates\"]}}]', 12, 1),
(80, '2019-08-20 16:08:24.371050', '9', 'Sagittarius', 2, '[{\"changed\": {\"fields\": [\"dates\"]}}]', 12, 1),
(81, '2019-08-20 16:09:33.757914', '10', 'Capricorn', 2, '[{\"changed\": {\"fields\": [\"dates\"]}}]', 12, 1),
(82, '2019-08-20 16:42:36.441954', '11', 'Aquarius', 2, '[{\"changed\": {\"fields\": [\"dates\"]}}]', 12, 1),
(83, '2019-08-20 16:42:49.157141', '12', 'Pisces', 2, '[{\"changed\": {\"fields\": [\"dates\"]}}]', 12, 1),
(84, '2019-08-21 11:18:59.377816', '3', 'News', 1, '[{\"added\": {}}]', 9, 1),
(85, '2019-08-21 11:19:26.247678', '3', 'News', 2, '[{\"changed\": {\"fields\": [\"image\"]}}]', 9, 1),
(86, '2019-08-21 11:35:45.616754', '3', 'das assd dassd', 2, '[{\"changed\": {\"fields\": [\"image\", \"title\"]}}]', 9, 1),
(87, '2019-08-21 11:36:11.199020', '3', 'das assd dassd', 3, '', 9, 1),
(88, '2019-08-21 13:09:59.563820', '4', 'Breaking News', 1, '[{\"added\": {}}]', 9, 1),
(89, '2019-08-21 13:10:17.612648', '4', 'Breaking News', 3, '', 9, 1),
(90, '2019-08-21 13:10:48.392427', '5', 'Breaking News', 1, '[{\"added\": {}}]', 9, 1),
(91, '2019-08-21 13:11:04.111689', '5', 'Breaking News', 2, '[{\"changed\": {\"fields\": [\"image\"]}}]', 9, 1),
(92, '2019-08-21 13:11:44.842211', '5', 'Breaking News', 3, '', 9, 1),
(93, '2019-08-21 13:12:19.352739', '6', 'Breaking News', 1, '[{\"added\": {}}]', 9, 1),
(94, '2019-08-21 13:12:26.595446', '6', 'Breaking News', 2, '[{\"changed\": {\"fields\": [\"image\"]}}]', 9, 1),
(95, '2019-08-21 13:12:40.696467', '7', 'Test nEws', 1, '[{\"added\": {}}]', 9, 1),
(96, '2019-08-21 13:12:53.097341', '7', 'Test nEws', 3, '', 9, 1),
(97, '2019-08-21 13:13:29.252343', '6', 'Breaking News', 3, '', 9, 1),
(98, '2019-08-21 13:33:27.695861', '8', 'Breaking News', 1, '[{\"added\": {}}]', 9, 1),
(99, '2019-08-21 13:34:16.694250', '8', 'sadsad  asda asdasd', 2, '[{\"changed\": {\"fields\": [\"title\"]}}]', 9, 1),
(100, '2019-08-21 13:34:41.941965', '8', 'sad asdnmbnmbnmntry ertretertret', 2, '[{\"changed\": {\"fields\": [\"title\"]}}]', 9, 1),
(101, '2019-08-21 13:40:31.935767', '8', 'Breaking News', 2, '[{\"changed\": {\"fields\": [\"title\"]}}]', 9, 1),
(102, '2019-08-21 13:45:57.636350', '9', 'Artificial Intelligence News', 1, '[{\"added\": {}}]', 9, 1),
(103, '2019-08-21 13:46:32.508467', '8', 'World-Wide News', 2, '[{\"changed\": {\"fields\": [\"title\"]}}]', 9, 1),
(104, '2019-08-21 15:07:15.178762', '9', 'Artificial Intelligence News', 2, '[{\"changed\": {\"fields\": [\"description\"]}}]', 9, 1),
(105, '2019-08-21 15:52:47.704812', '7', 'News', 1, '[{\"added\": {}}]', 7, 1),
(106, '2019-08-21 16:40:11.211680', '1', 'About', 2, '[{\"changed\": {\"fields\": [\"order_by\"]}}]', 7, 1),
(107, '2019-08-21 16:40:21.787917', '5', 'Daily Horoscope', 2, '[{\"changed\": {\"fields\": [\"order_by\"]}}]', 7, 1),
(108, '2019-08-21 16:40:25.218355', '7', 'News', 2, '[{\"changed\": {\"fields\": [\"order_by\"]}}]', 7, 1),
(109, '2019-08-21 16:40:30.204899', '7', 'News', 2, '[{\"changed\": {\"fields\": [\"order_by\"]}}]', 7, 1),
(110, '2019-08-21 16:40:33.217056', '5', 'Daily Horoscope', 2, '[{\"changed\": {\"fields\": [\"order_by\"]}}]', 7, 1),
(111, '2019-08-21 16:40:38.465772', '6', 'Contact', 2, '[{\"changed\": {\"fields\": [\"order_by\"]}}]', 7, 1),
(112, '2019-08-21 16:42:21.841198', '8', 'Test News', 1, '[{\"added\": {}}]', 7, 1),
(113, '2019-08-21 16:43:13.241805', '8', 'Test News', 3, '', 7, 1),
(114, '2019-08-21 16:47:01.770259', '4', 'This is news article 1', 1, '[{\"added\": {}}]', 8, 1),
(115, '2019-08-21 16:54:17.996276', '4', 'This is news article 1', 2, '[{\"changed\": {\"fields\": [\"description\"]}}]', 8, 1),
(116, '2019-08-21 17:23:35.350892', '4', 'This is news article 1', 2, '[{\"changed\": {\"fields\": [\"description\"]}}]', 8, 1),
(117, '2019-08-21 17:25:53.677797', '5', 'This is news article 2', 1, '[{\"added\": {}}]', 8, 1),
(118, '2019-08-21 17:27:26.134094', '5', 'This is news article 2', 2, '[{\"changed\": {\"fields\": [\"files\"]}}]', 8, 1),
(119, '2019-08-21 18:44:50.678014', '6', 'This is news article 3', 1, '[{\"added\": {}}]', 8, 1),
(120, '2019-08-21 18:46:17.134052', '7', 'This is a test title 4', 1, '[{\"added\": {}}]', 8, 1),
(121, '2019-08-21 18:46:39.282505', '8', 'This is a test title 5', 1, '[{\"added\": {}}]', 8, 1),
(122, '2019-08-21 18:46:56.916609', '8', 'This is news article 5', 2, '[{\"changed\": {\"fields\": [\"title\"]}}]', 8, 1),
(123, '2019-08-21 18:49:45.734325', '8', 'This is news article 5', 2, '[]', 8, 1),
(124, '2019-08-21 18:49:57.885022', '7', 'This is news article 5', 2, '[{\"changed\": {\"fields\": [\"title\"]}}]', 8, 1),
(125, '2019-08-21 18:50:33.784537', '7', 'This is news article 4', 2, '[{\"changed\": {\"fields\": [\"title\", \"slug\"]}}]', 8, 1),
(126, '2019-08-21 18:51:16.856170', '9', 'This is news article 7', 1, '[{\"added\": {}}]', 8, 1),
(127, '2019-08-21 18:51:28.165189', '9', 'This is news article 6', 2, '[{\"changed\": {\"fields\": [\"title\", \"slug\"]}}]', 8, 1),
(128, '2019-08-21 18:58:54.247489', '9', 'Sports', 1, '[{\"added\": {}}]', 7, 1),
(129, '2019-08-21 18:59:17.785283', '10', 'Business', 1, '[{\"added\": {}}]', 7, 1),
(130, '2019-08-21 18:59:31.461022', '11', 'Technology', 1, '[{\"added\": {}}]', 7, 1),
(131, '2019-08-21 19:00:43.161967', '12', 'Fashion', 1, '[{\"added\": {}}]', 7, 1),
(132, '2019-08-21 19:01:04.456101', '13', 'Life & Style', 1, '[{\"added\": {}}]', 7, 1),
(133, '2019-08-21 19:01:41.041392', '14', 'Photography', 1, '[{\"added\": {}}]', 7, 1),
(134, '2019-08-22 15:06:43.818287', '7', 'News', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(135, '2019-08-22 15:08:36.229932', '7', 'News', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(136, '2019-08-22 15:24:37.630671', '4', 'This is news article 1', 2, '[{\"changed\": {\"fields\": [\"image\"]}}]', 8, 1),
(137, '2019-08-22 16:02:28.468746', '1', 'About Us', 2, '[{\"changed\": {\"fields\": [\"description\"]}}]', 8, 1),
(138, '2019-08-22 16:11:57.666074', '14', 'Photography', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(139, '2019-08-22 16:12:03.176817', '13', 'Life & Style', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(140, '2019-08-22 16:12:07.293743', '12', 'Fashion', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(141, '2019-08-22 16:12:11.400661', '11', 'Technology', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(142, '2019-08-22 16:12:15.621662', '10', 'Business', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(143, '2019-08-22 16:12:19.376332', '9', 'Sports', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(144, '2019-08-23 12:13:25.344167', '7', 'News', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(145, '2019-08-23 12:13:51.811837', '7', 'News', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(146, '2019-08-23 14:43:21.248660', '7', 'News', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(147, '2019-08-23 14:53:35.576255', '7', 'News', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(148, '2019-08-23 14:57:30.113628', '7', 'News', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(149, '2019-08-23 15:00:07.924638', '7', 'News', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(150, '2019-08-23 16:39:25.570245', '10', 'This is fashion article 1', 1, '[{\"added\": {}}]', 8, 1),
(151, '2019-08-23 16:53:19.528951', '12', 'Fashion', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(152, '2019-08-23 23:13:38.614621', '14', 'Photography', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(153, '2019-08-23 23:24:48.228506', '11', 'This is photography content 1', 1, '[{\"added\": {}}]', 8, 1),
(154, '2019-08-23 23:25:08.009619', '12', 'This is photography content 2', 1, '[{\"added\": {}}]', 8, 1),
(155, '2019-08-23 23:25:18.294490', '13', 'This is photography content 3', 1, '[{\"added\": {}}]', 8, 1),
(156, '2019-08-23 23:25:39.483296', '14', 'This is photography content 4', 1, '[{\"added\": {}}]', 8, 1),
(157, '2019-08-23 23:25:48.963053', '15', 'This is photography content 5', 1, '[{\"added\": {}}]', 8, 1),
(158, '2019-08-23 23:25:58.749991', '16', 'This is photography content 6', 1, '[{\"added\": {}}]', 8, 1),
(159, '2019-08-23 23:26:07.950947', '17', 'This is photography content 7', 1, '[{\"added\": {}}]', 8, 1),
(160, '2019-08-23 23:26:17.217533', '18', 'This is photography content 8', 1, '[{\"added\": {}}]', 8, 1),
(161, '2019-08-28 14:11:25.254993', '15', 'FAQ', 1, '[{\"added\": {}}]', 7, 1),
(162, '2019-08-28 17:22:14.746662', '1', 'This is FAQ question 1?', 1, '[{\"added\": {}}]', 16, 1),
(163, '2019-08-28 17:46:30.437631', '2', 'This is FAQ question 2?', 1, '[{\"added\": {}}]', 16, 1),
(164, '2019-08-28 17:50:42.416532', '2', 'This is FAQ question 2?', 2, '[{\"changed\": {\"fields\": [\"answer\"]}}]', 16, 1),
(165, '2019-08-28 18:01:40.050971', '2', 'This is FAQ question 2?', 2, '[{\"changed\": {\"fields\": [\"order_by\"]}}]', 16, 1),
(166, '2019-08-28 18:01:44.781333', '1', 'This is FAQ question 1?', 2, '[{\"changed\": {\"fields\": [\"order_by\"]}}]', 16, 1),
(167, '2019-08-28 18:02:59.088851', '15', 'FAQ', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 7, 1),
(168, '2019-08-28 18:03:31.293741', '15', 'FAQ', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 7, 1),
(169, '2019-08-28 18:08:31.658125', '7', 'News', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(170, '2019-08-28 18:11:12.625276', '7', 'News', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(171, '2019-08-28 23:06:21.755335', '1', 'Pathao', 1, '[{\"added\": {}}]', 17, 1),
(172, '2019-08-28 23:07:32.879947', '2', 'Ncell', 1, '[{\"added\": {}}]', 17, 1),
(173, '2019-08-28 23:07:44.680781', '3', 'Esewa', 1, '[{\"added\": {}}]', 17, 1),
(174, '2019-08-28 23:07:54.135500', '4', 'The World Bank', 1, '[{\"added\": {}}]', 17, 1),
(175, '2019-08-29 00:21:21.128307', '2', 'Popup object (2)', 1, '[{\"added\": {}}]', 18, 1),
(176, '2019-08-29 09:14:37.941708', '3', 'Popup object (3)', 1, '[{\"added\": {}}]', 18, 1),
(177, '2019-08-29 11:08:59.927184', '3', 'Popup object (3)', 2, '[{\"changed\": {\"fields\": [\"link\", \"link_name\"]}}]', 18, 1),
(178, '2019-08-29 11:09:33.329663', '3', 'Popup object (3)', 2, '[{\"changed\": {\"fields\": [\"link\", \"link_name\"]}}]', 18, 1),
(179, '2019-08-29 14:27:06.906128', '3', 'This is FAQ question 3?', 1, '[{\"added\": {}}]', 16, 1),
(180, '2019-08-29 14:27:19.857334', '4', 'This is FAQ question 4?', 1, '[{\"added\": {}}]', 16, 1),
(181, '2019-08-29 14:27:42.329305', '5', 'This is FAQ question 5?', 1, '[{\"added\": {}}]', 16, 1),
(182, '2019-08-29 14:42:15.696742', '7', 'News', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(183, '2019-08-29 14:42:29.106293', '7', 'News', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(184, '2019-08-29 14:42:41.602156', '7', 'News', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(185, '2019-08-29 14:43:03.473701', '7', 'News', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(186, '2019-08-29 14:43:34.503757', '7', 'News', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(187, '2019-08-29 14:43:44.825092', '7', 'News', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(188, '2019-09-09 14:21:04.452009', '16', 'Test Main menu', 1, '[{\"added\": {}}]', 7, 1),
(189, '2019-09-09 14:30:22.543970', '17', 'Test Sub Menu', 1, '[{\"added\": {}}]', 7, 1),
(190, '2019-09-09 14:30:46.199784', '17', 'Test Sub Menu', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 7, 1),
(191, '2019-09-09 14:43:12.643283', '17', 'Test Sub Menu', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 7, 1),
(192, '2019-09-09 14:43:52.480599', '16', 'Test Main menu', 2, '[{\"changed\": {\"fields\": [\"status\"]}}]', 7, 1),
(193, '2019-09-10 09:22:53.240134', '7', 'Fox News', 2, '[]', 10, 1),
(194, '2019-09-13 15:11:27.122378', '17', 'Test Sub Menu', 3, '', 7, 1),
(195, '2019-09-13 15:11:27.236458', '16', 'Test Main menu', 3, '', 7, 1),
(196, '2019-09-13 15:12:14.139796', '18', 'Universities', 1, '[{\"added\": {}}]', 7, 1),
(197, '2019-09-13 15:14:36.472078', '19', 'Kathmandu University', 1, '[{\"added\": {}}]', 7, 1),
(198, '2019-09-13 15:15:55.062446', '19', 'About KU', 1, '[{\"added\": {}}]', 8, 1),
(199, '2019-09-13 15:18:15.805525', '19', 'About KU', 2, '[{\"changed\": {\"fields\": [\"description\"]}}]', 8, 1),
(200, '2019-09-13 15:21:04.128648', '19', 'About KU', 2, '[{\"changed\": {\"fields\": [\"description\"]}}]', 8, 1),
(201, '2019-09-13 15:21:45.134331', '20', 'Tribhuvan University', 1, '[{\"added\": {}}]', 7, 1),
(202, '2019-09-13 15:22:04.879048', '20', 'About TU', 1, '[{\"added\": {}}]', 8, 1),
(203, '2019-09-13 15:23:56.051103', '20', 'Tribhuvan University', 2, '[{\"changed\": {\"fields\": [\"is_main_menu\", \"parent_menu\"]}}]', 7, 1),
(204, '2019-09-13 15:38:00.469224', '20', 'About TU', 2, '[{\"changed\": {\"fields\": [\"description\"]}}]', 8, 1),
(205, '2019-09-13 15:38:52.153961', '19', 'About KU', 2, '[{\"changed\": {\"fields\": [\"description\"]}}]', 8, 1),
(206, '2019-09-13 15:40:22.076744', '19', 'About KU', 2, '[{\"changed\": {\"fields\": [\"description\"]}}]', 8, 1),
(207, '2019-09-13 15:40:56.837861', '20', 'Tribhuvan University', 2, '[{\"changed\": {\"fields\": [\"is_main_menu\", \"parent_menu\"]}}]', 7, 1),
(208, '2019-09-13 15:41:37.641484', '20', 'Tribhuvan University', 2, '[{\"changed\": {\"fields\": [\"page_type\"]}}]', 7, 1),
(209, '2019-09-13 15:41:58.950352', '20', 'Prime College', 2, '[{\"changed\": {\"fields\": [\"title\"]}}]', 8, 1),
(210, '2019-09-13 15:42:18.129239', '20', 'Prime College', 2, '[{\"changed\": {\"fields\": [\"description\"]}}]', 8, 1),
(211, '2019-09-13 15:44:12.915600', '21', 'Kathmandu Engineering College', 1, '[{\"added\": {}}]', 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(5, 'contenttypes', 'contenttype'),
(16, 'newsapp', 'faq'),
(12, 'newsapp', 'horoscope'),
(9, 'newsapp', 'imageslider'),
(7, 'newsapp', 'menu'),
(8, 'newsapp', 'menucontent'),
(14, 'newsapp', 'newsarticle'),
(13, 'newsapp', 'newsarticlecategory'),
(18, 'newsapp', 'popup'),
(10, 'newsapp', 'quicklinks'),
(15, 'newsapp', 'siteconfig'),
(11, 'newsapp', 'sociallinks'),
(17, 'newsapp', 'sponsor'),
(6, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2019-08-06 06:41:57.588749'),
(2, 'auth', '0001_initial', '2019-08-06 06:41:59.656238'),
(3, 'admin', '0001_initial', '2019-08-06 06:42:11.694776'),
(4, 'admin', '0002_logentry_remove_auto_add', '2019-08-06 06:42:14.156525'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2019-08-06 06:42:14.229578'),
(6, 'contenttypes', '0002_remove_content_type_name', '2019-08-06 06:42:17.233712'),
(7, 'auth', '0002_alter_permission_name_max_length', '2019-08-06 06:42:20.033702'),
(8, 'auth', '0003_alter_user_email_max_length', '2019-08-06 06:42:25.788809'),
(9, 'auth', '0004_alter_user_username_opts', '2019-08-06 06:42:25.874854'),
(10, 'auth', '0005_alter_user_last_login_null', '2019-08-06 06:42:28.167483'),
(11, 'auth', '0006_require_contenttypes_0002', '2019-08-06 06:42:28.243557'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2019-08-06 06:42:28.283566'),
(13, 'auth', '0008_alter_user_username_max_length', '2019-08-06 06:42:29.543481'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2019-08-06 06:42:30.966473'),
(15, 'auth', '0010_alter_group_name_max_length', '2019-08-06 06:42:32.099278'),
(16, 'auth', '0011_update_proxy_permissions', '2019-08-06 06:42:32.143311'),
(17, 'newsapp', '0001_initial', '2019-08-06 06:42:34.596052'),
(18, 'sessions', '0001_initial', '2019-08-06 06:42:54.972260'),
(19, 'newsapp', '0002_quicklinks', '2019-08-10 23:10:09.415183'),
(20, 'newsapp', '0003_auto_20190811_1135', '2019-08-11 11:35:27.613328'),
(21, 'newsapp', '0004_horoscope', '2019-08-11 15:03:58.445299'),
(22, 'newsapp', '0005_newsarticlecategory', '2019-08-11 23:00:49.635715'),
(23, 'newsapp', '0006_newsarticle_siteconfig', '2019-08-19 23:41:44.568408'),
(24, 'newsapp', '0007_auto_20190819_2343', '2019-08-19 23:43:55.203812'),
(25, 'newsapp', '0008_auto_20190820_1007', '2019-08-20 10:08:12.736827'),
(26, 'newsapp', '0009_horoscope_dates', '2019-08-20 16:05:50.661065'),
(27, 'newsapp', '0002_auto_20190821_1117', '2019-08-21 11:17:42.872559'),
(28, 'newsapp', '0002_menu_order_by', '2019-08-21 16:38:31.533169'),
(29, 'newsapp', '0003_remove_menucontent_description', '2019-08-21 16:58:36.628187'),
(30, 'newsapp', '0004_menucontent_description', '2019-08-21 17:00:06.687266'),
(31, 'newsapp', '0005_remove_menucontent_description', '2019-08-21 17:11:47.946246'),
(32, 'newsapp', '0006_menucontent_description', '2019-08-21 17:11:48.081343'),
(33, 'newsapp', '0007_auto_20190821_1712', '2019-08-21 17:12:36.713909'),
(34, 'newsapp', '0008_auto_20190821_1721', '2019-08-21 17:21:46.185427'),
(35, 'newsapp', '0009_auto_20190821_1722', '2019-08-21 17:22:14.165828'),
(36, 'newsapp', '0002_auto_20190821_1905', '2019-08-21 19:13:23.547879'),
(37, 'newsapp', '0003_auto_20190821_1907', '2019-08-21 19:13:23.651952'),
(38, 'newsapp', '0004_auto_20190821_1909', '2019-08-21 19:13:23.716999'),
(39, 'newsapp', '0005_auto_20190821_1909', '2019-08-21 19:13:23.817070'),
(40, 'newsapp', '0002_auto_20190822_1546', '2019-08-22 15:46:56.814200'),
(41, 'newsapp', '0003_auto_20190823_2324', '2019-08-23 23:24:41.238466'),
(42, 'newsapp', '0004_faq', '2019-08-28 16:36:39.792607'),
(43, 'newsapp', '0002_faq_order_by', '2019-08-28 17:31:47.950801'),
(44, 'newsapp', '0003_auto_20190828_1805', '2019-08-28 18:05:57.478645'),
(45, 'newsapp', '0004_sponsor', '2019-08-28 23:01:20.152841'),
(46, 'newsapp', '0005_auto_20190828_2335', '2019-08-28 23:35:07.500593'),
(47, 'newsapp', '0006_popup', '2019-08-28 23:55:07.103497');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('jry4g9ebfp87065p10fb3g5vyogna99c', 'YmE2NDg2MmQ2NDFlMDNkM2Q0MjdjMTY3YmZmZTczMjgyZGE2M2ZmYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3ODNhOGU5ZDlhNDQwZjAxNjJiY2FlYzQ5ZTA5ZDA0ZWI2MTgyODhkIn0=', '2019-08-23 15:13:13.080648'),
('k1p4zzr7pziv1tt6nc619729iwshucuh', 'YmE2NDg2MmQ2NDFlMDNkM2Q0MjdjMTY3YmZmZTczMjgyZGE2M2ZmYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3ODNhOGU5ZDlhNDQwZjAxNjJiY2FlYzQ5ZTA5ZDA0ZWI2MTgyODhkIn0=', '2019-09-04 09:01:17.155502'),
('rwbiuslnflzem13a85l85dl9rv2sqo9h', 'YmE2NDg2MmQ2NDFlMDNkM2Q0MjdjMTY3YmZmZTczMjgyZGE2M2ZmYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3ODNhOGU5ZDlhNDQwZjAxNjJiY2FlYzQ5ZTA5ZDA0ZWI2MTgyODhkIn0=', '2019-09-24 07:53:38.804767'),
('tjuelblw6rt9rjvgexwkvyyq46v6zfnc', 'YmE2NDg2MmQ2NDFlMDNkM2Q0MjdjMTY3YmZmZTczMjgyZGE2M2ZmYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3ODNhOGU5ZDlhNDQwZjAxNjJiY2FlYzQ5ZTA5ZDA0ZWI2MTgyODhkIn0=', '2019-10-09 14:08:10.782999');

-- --------------------------------------------------------

--
-- Table structure for table `newsapp_faq`
--

CREATE TABLE `newsapp_faq` (
  `id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer` longtext NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `created_by_id` int(11) NOT NULL,
  `order_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsapp_faq`
--

INSERT INTO `newsapp_faq` (`id`, `question`, `answer`, `status`, `created_at`, `created_by_id`, `order_by`) VALUES
(1, 'This is FAQ question 1?', '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam ratione ex inventore harum, totam, voluptatibus corporis obcaecati veniam reprehenderit accusantium ipsam exercitationem officiis? Tempora itaque aliquid debitis accusamus quaerat explicabo.Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam ratione ex inventore harum, totam, voluptatibus corporis obcaecati veniam reprehenderit accusantium ipsam exercitationem officiis? Tempora itaque aliquid debitis accusamus quaerat explicabo.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam ratione ex inventore harum, totam, voluptatibus corporis obcaecati veniam reprehenderit accusantium ipsam exercitationem officiis? Tempora itaque aliquid debitis accusamus quaerat explicabo.</p>', 1, '2019-08-28 17:22:14.690623', 1, 1),
(2, 'This is FAQ question 2?', '<ol>\r\n	<li>\r\n	<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Architecto vero dignissimos ea? Labore, officia dignissimos vitae, tempore doloremque et nobis accusamus nemo numquam consectetur, itaque sunt. Fuga aliquam odio iusto.</p>\r\n	</li>\r\n	<li>\r\n	<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Architecto vero dignissimos ea? Labore, officia dignissimos vitae, tempore doloremque et nobis accusamus nemo numquam consectetur, itaque sunt. Fuga aliquam odio iusto.</p>\r\n	</li>\r\n	<li>\r\n	<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Architecto vero dignissimos ea? Labore, officia dignissimos vitae, tempore doloremque et nobis accusamus nemo numquam consectetur, itaque sunt. Fuga aliquam odio iusto.</p>\r\n	</li>\r\n	<li>\r\n	<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Architecto vero dignissimos ea? Labore, officia dignissimos vitae, tempore doloremque et nobis accusamus nemo numquam consectetur, itaque sunt. Fuga aliquam odio iusto.</p>\r\n	</li>\r\n</ol>', 1, '2019-08-28 17:46:30.294530', 1, 2),
(3, 'This is FAQ question 3?', '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea odit asperiores necessitatibus dicta, recusandae, quae aspernatur beatae qui totam illum, adipisci fugit! Et animi in veniam sequi, fugiat atque consequatur.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea odit asperiores necessitatibus dicta, recusandae, quae aspernatur beatae qui totam illum, adipisci fugit! Et animi in veniam sequi, fugiat atque consequatur.</p>', 1, '2019-08-29 14:27:06.727003', 1, 3),
(4, 'This is FAQ question 4?', '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea odit asperiores necessitatibus dicta, recusandae, quae aspernatur beatae qui totam illum, adipisci fugit! Et animi in veniam sequi, fugiat atque consequatur.Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea odit asperiores necessitatibus dicta, recusandae, quae aspernatur beatae qui totam illum, adipisci fugit! Et animi in veniam sequi, fugiat atque consequatur.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea odit asperiores necessitatibus dicta, recusandae, quae aspernatur beatae qui totam illum, adipisci fugit! Et animi in veniam sequi, fugiat atque consequatur.</p>', 1, '2019-08-29 14:27:19.836320', 1, 4),
(5, 'This is FAQ question 5?', '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea odit asperiores necessitatibus dicta, recusandae, quae aspernatur beatae qui totam illum, adipisci fugit! Et animi in veniam sequi, fugiat atque consequatur.Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea odit asperiores necessitatibus dicta, recusandae, quae aspernatur beatae qui totam illum, adipisci fugit! Et animi in veniam sequi, fugiat atque consequatur.</p>\r\n\r\n<ol>\r\n	<li>\r\n	<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea odit asperiores necessitatibus dicta, recusandae, quae aspernatur beatae qui totam illum, adipisci fugit! Et animi in veniam sequi, fugiat atque consequatur.</p>\r\n	</li>\r\n	<li>\r\n	<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea odit asperiores necessitatibus dicta, recusandae, quae aspernatur beatae qui totam illum, adipisci fugit! Et animi in veniam sequi, fugiat atque consequatur.</p>\r\n	</li>\r\n	<li>\r\n	<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea odit asperiores necessitatibus dicta, recusandae, quae aspernatur beatae qui totam illum, adipisci fugit! Et animi in veniam sequi, fugiat atque consequatur.</p>\r\n	</li>\r\n	<li>\r\n	<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea odit asperiores necessitatibus dicta, recusandae, quae aspernatur beatae qui totam illum, adipisci fugit! Et animi in veniam sequi, fugiat atque consequatur.</p>\r\n	</li>\r\n</ol>\r\n\r\n<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea odit asperiores necessitatibus dicta, recusandae, quae aspernatur beatae qui totam illum, adipisci fugit! Et animi in veniam sequi, fugiat atque consequatur.Lorem ipsum dolor sit amet consectetur adipisicing elit. Ea odit asperiores necessitatibus dicta, recusandae, quae aspernatur beatae qui totam illum, adipisci fugit! Et animi in veniam sequi, fugiat atque consequatur.</p>', 1, '2019-08-29 14:27:42.253252', 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `newsapp_horoscope`
--

CREATE TABLE `newsapp_horoscope` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `order_by` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `dates` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsapp_horoscope`
--

INSERT INTO `newsapp_horoscope` (`id`, `name`, `image`, `order_by`, `status`, `dates`) VALUES
(1, 'Aries', 'images/horoscope/aries.jpg', 1, 1, 'March 21 - April 19'),
(2, 'Taurus', 'images/horoscope/taurus.jpg', 2, 1, 'April 20 - May 20'),
(3, 'Gemini', 'images/horoscope/gemini.jpg', 3, 1, 'May 21 - June 20'),
(4, 'Cancer', 'images/horoscope/cancer.jpg', 4, 1, 'June 21 - July 22'),
(5, 'Leo', 'images/horoscope/leo.jpg', 5, 1, 'July 23 - August 22'),
(6, 'Virgo', 'images/horoscope/virgo.jpg', 6, 1, 'August 23 - September 22'),
(7, 'Libra', 'images/horoscope/libra.jpg', 7, 1, 'September 23 - October 22'),
(8, 'Scorpio', 'images/horoscope/scorpio.jpg', 8, 1, 'October 23 - November 21'),
(9, 'Sagittarius', 'images/horoscope/sagittarius.jpg', 9, 1, 'November 22 - December 21'),
(10, 'Capricorn', 'images/horoscope/capricorn.jpg', 10, 1, 'December 22 - January 19'),
(11, 'Aquarius', 'images/horoscope/aquarius.jpg', 11, 1, 'January 20 - February 18'),
(12, 'Pisces', 'images/horoscope/pisces.jpg', 12, 1, 'February 19 - March 20');

-- --------------------------------------------------------

--
-- Table structure for table `newsapp_imageslider`
--

CREATE TABLE `newsapp_imageslider` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `title` varchar(200) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` longtext DEFAULT NULL,
  `order_by` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `created_by_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsapp_imageslider`
--

INSERT INTO `newsapp_imageslider` (`id`, `image`, `title`, `slug`, `description`, `order_by`, `status`, `created_at`, `created_by_id`) VALUES
(8, 'images/slider/banner-news.jpg', 'World-Wide News', 'world-wide-news', '', 1, 1, '2019-08-21 13:33:27.673846', 1),
(9, 'images/slider/news-event-banner1.jpg', 'Artificial Intelligence News', 'artificial-intelligence-news', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo, fuga nulla ab ut quis cumque voluptatum, facere amet, totam molestias quasi animi soluta quibusdam numquam. Fuga maxime quam quod repellendus!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo, fuga nulla ab ut quis cumque voluptatum, facere amet, totam molestias quasi animi soluta quibusdam numquam. Fuga maxime quam quod repellendus!</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo, fuga nulla ab ut quis cumque voluptatum, facere amet, totam molestias quasi animi soluta quibusdam numquam. Fuga maxime quam quod repellendus!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo, fuga nulla ab ut quis cumque voluptatum, facere amet, totam molestias quasi animi soluta quibusdam numquam. Fuga maxime quam quod repellendus!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo, fuga nulla ab ut quis cumque voluptatum, facere amet, totam molestias quasi animi soluta quibusdam numquam. Fuga maxime quam quod repellendus!</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo, fuga nulla ab ut quis cumque voluptatum, facere amet, totam molestias quasi animi soluta quibusdam numquam. Fuga maxime quam quod repellendus!</p>', 2, 1, '2019-08-21 13:45:57.618338', 1);

-- --------------------------------------------------------

--
-- Table structure for table `newsapp_menu`
--

CREATE TABLE `newsapp_menu` (
  `id` int(11) NOT NULL,
  `is_main_menu` varchar(10) NOT NULL,
  `menu_name` varchar(100) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `page_type` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `created_by_id` int(11) NOT NULL,
  `parent_menu_id` int(11) DEFAULT NULL,
  `order_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsapp_menu`
--

INSERT INTO `newsapp_menu` (`id`, `is_main_menu`, `menu_name`, `slug`, `page_type`, `status`, `created_at`, `created_by_id`, `parent_menu_id`, `order_by`) VALUES
(1, 'y', 'About', 'about', 'dp', 1, '2019-08-06 07:16:31.685399', 1, NULL, 1),
(5, 'y', 'Daily Horoscope', 'daily-horoscope', 'dp', 1, '2019-08-11 13:18:20.970856', 1, NULL, 3),
(6, 'y', 'Contact', 'contact', 'dp', 1, '2019-08-19 22:51:10.079049', 1, NULL, 999),
(7, 'y', 'News', 'news', 'tlp', 1, '2019-08-21 15:52:47.672789', 1, NULL, 2),
(9, 'y', 'Sports', 'sports', 'tlp', 1, '2019-08-21 18:58:54.202459', 1, NULL, 4),
(10, 'y', 'Business', 'business', 'tlp', 1, '2019-08-21 18:59:17.753260', 1, NULL, 5),
(11, 'y', 'Technology', 'technology', 'tlp', 1, '2019-08-21 18:59:31.426981', 1, NULL, 6),
(12, 'y', 'Fashion', 'fashion', 'tlp', 1, '2019-08-21 19:00:43.106928', 1, NULL, 7),
(13, 'y', 'Life & Style', 'life-style', 'slp', 1, '2019-08-21 19:01:04.425081', 1, NULL, 8),
(14, 'y', 'Photography', 'photography', 'gp', 1, '2019-08-21 19:01:41.003345', 1, NULL, 9),
(15, 'y', 'FAQ', 'faq', 'dp', 1, '2019-08-28 14:11:25.045825', 1, NULL, 888),
(18, 'y', 'Universities', 'universities', 'dp', 0, '2019-09-13 15:12:14.089761', 1, NULL, 9999),
(19, 'n', 'Kathmandu University', 'kathmandu-university', 'dp', 0, '2019-09-13 15:14:36.430050', 1, 18, 99999),
(20, 'n', 'Tribhuvan University', 'tribhuvan-university', 'slp', 0, '2019-09-13 15:21:45.104312', 1, 18, 9999);

-- --------------------------------------------------------

--
-- Table structure for table `newsapp_menucontent`
--

CREATE TABLE `newsapp_menucontent` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `files` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `created_by_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `description` longtext DEFAULT NULL,
  `visited` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsapp_menucontent`
--

INSERT INTO `newsapp_menucontent` (`id`, `title`, `slug`, `image`, `files`, `status`, `created_at`, `created_by_id`, `menu_id`, `description`, `visited`) VALUES
(1, 'About Us', 'about-us', '', '', 1, '2019-08-09 12:05:37.415984', 1, 1, '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. At quis accusamus dignissimos molestiae, ea incidunt blanditiis expedita animi debitis repellat, nostrum nam quod earum, reprehenderit velit laboriosam autem ipsam vero.Lorem ipsum dolor sit amet consectetur adipisicing elit. At quis accusamus dignissimos molestiae, ea incidunt blanditiis expedita animi debitis repellat, nostrum nam quod earum, reprehenderit velit laboriosam autem ipsam vero.</p>\r\n\r\n<p><img alt=\"\" src=\"/media/uploads/ckeditor/2019/08/22/programming-logic-key-ingredients-banner.png\" style=\"height:133px; width:500px\" /></p>\r\n\r\n<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. At quis accusamus dignissimos molestiae, ea incidunt blanditiis expedita animi debitis repellat, nostrum nam quod earum, reprehenderit velit laboriosam autem ipsam vero.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. At quis accusamus dignissimos molestiae, ea incidunt blanditiis expedita animi debitis repellat, nostrum nam quod earum, reprehenderit velit laboriosam autem ipsam vero.Lorem ipsum dolor sit amet consectetur adipisicing elit. At quis accusamus dignissimos molestiae, ea incidunt blanditiis expedita animi debitis repellat, nostrum nam quod earum, reprehenderit velit laboriosam autem ipsam vero.Lorem ipsum dolor sit amet consectetur adipisicing elit. At quis accusamus dignissimos molestiae, ea incidunt blanditiis expedita animi debitis repellat, nostrum nam quod earum, reprehenderit velit laboriosam autem ipsam vero.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. At quis accusamus dignissimos molestiae, ea incidunt blanditiis expedita animi debitis repellat, nostrum nam quod earum, reprehenderit velit laboriosam autem ipsam vero.</p>', 72),
(4, 'This is news article 1', 'this-is-news-article-1', 'images/menu/python-development-service-banner.png', '', 1, '2019-08-21 16:47:01.710215', 1, 7, '<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.</p>\r\n\r\n<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.</p>\r\n\r\n<p><img alt=\"\" src=\"/media/uploads/ckeditor/2019/08/21/banner-news.jpg\" style=\"height:250px; width:500px\" /></p>\r\n\r\n<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.</p>\r\n\r\n<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.</p>\r\n\r\n<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.</p>\r\n\r\n<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.</p>', 9),
(5, 'This is news article 2', 'this-is-news-article-2', '', 'files/menu/text-extraction-from-natural-sceneimages-and-conversion-to-audio-in-smartphone-applications.pdf', 1, '2019-08-21 17:25:53.643756', 1, 7, '<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.</p>\r\n\r\n<p><img alt=\"\" src=\"/media/uploads/ckeditor/2019/08/21/whichprogramminglanguage_banner.jpg\" style=\"height:250px; width:478px\" /></p>\r\n\r\n<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.</p>\r\n\r\n<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.</p>\r\n\r\n<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.</p>\r\n\r\n<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.</p>\r\n\r\n<p><img alt=\"\" src=\"/media/uploads/ckeditor/2019/08/21/python-development-service-banner.png\" style=\"height:250px; width:276px\" /></p>\r\n\r\n<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatum quisquam dolorum exercitationem possimus repellendus eos nesciunt sed sapiente earum, doloribus dolorem natus iure beatae dolores voluptatibus! Adipisci modi sapiente quas.</p>', 5),
(6, 'This is news article 3', 'this-is-news-article-3', '', '', 1, '2019-08-21 18:44:50.677012', 1, 7, '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum sapiente ea tempora esse laborum, corrupti, nam debitis reiciendis quam aliquam quidem officia laboriosam a facere omnis amet nisi explicabo quia.Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum sapiente ea tempora esse laborum, corrupti, nam debitis reiciendis quam aliquam quidem officia laboriosam a facere omnis amet nisi explicabo quia.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum sapiente ea tempora esse laborum, corrupti, nam debitis reiciendis quam aliquam quidem officia laboriosam a facere omnis amet nisi explicabo quia.Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum sapiente ea tempora esse laborum, corrupti, nam debitis reiciendis quam aliquam quidem officia laboriosam a facere omnis amet nisi explicabo quia.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum sapiente ea tempora esse laborum, corrupti, nam debitis reiciendis quam aliquam quidem officia laboriosam a facere omnis amet nisi explicabo quia.</p>', 2),
(7, 'This is news article 4', 'this-is-news-article-4', '', '', 1, '2019-08-21 18:46:17.091021', 1, 7, '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum sapiente ea tempora esse laborum, corrupti, nam debitis reiciendis quam aliquam quidem officia laboriosam a facere omnis amet nisi explicabo quia.Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum sapiente ea tempora esse laborum, corrupti, nam debitis reiciendis quam aliquam quidem officia laboriosam a facere omnis amet nisi explicabo quia.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum sapiente ea tempora esse laborum, corrupti, nam debitis reiciendis quam aliquam quidem officia laboriosam a facere omnis amet nisi explicabo quia.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum sapiente ea tempora esse laborum, corrupti, nam debitis reiciendis quam aliquam quidem officia laboriosam a facere omnis amet nisi explicabo quia.Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum sapiente ea tempora esse laborum, corrupti, nam debitis reiciendis quam aliquam quidem officia laboriosam a facere omnis amet nisi explicabo quia.</p>', 4),
(8, 'This is news article 5', 'this-is-news-article-5', '', '', 1, '2019-08-21 18:46:39.240474', 1, 7, '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum sapiente ea tempora esse laborum, corrupti, nam debitis reiciendis quam aliquam quidem officia laboriosam a facere omnis amet nisi explicabo quia.Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum sapiente ea tempora esse laborum, corrupti, nam debitis reiciendis quam aliquam quidem officia laboriosam a facere omnis amet nisi explicabo quia.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum sapiente ea tempora esse laborum, corrupti, nam debitis reiciendis quam aliquam quidem officia laboriosam a facere omnis amet nisi explicabo quia.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum sapiente ea tempora esse laborum, corrupti, nam debitis reiciendis quam aliquam quidem officia laboriosam a facere omnis amet nisi explicabo quia.Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum sapiente ea tempora esse laborum, corrupti, nam debitis reiciendis quam aliquam quidem officia laboriosam a facere omnis amet nisi explicabo quia.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum sapiente ea tempora esse laborum, corrupti, nam debitis reiciendis quam aliquam quidem officia laboriosam a facere omnis amet nisi explicabo quia.</p>', 3),
(9, 'This is news article 6', 'this-is-news-article-6', '', '', 1, '2019-08-21 18:51:16.818126', 1, 7, '<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat quia natus velit architecto, quo ab perferendis accusamus modi reprehenderit eos doloribus aliquam nam beatae earum? Eum laudantium blanditiis minus vero?Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat quia natus velit architecto, quo ab perferendis accusamus modi reprehenderit eos doloribus aliquam nam beatae earum? Eum laudantium blanditiis minus vero?</p>\r\n\r\n<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat quia natus velit architecto, quo ab perferendis accusamus modi reprehenderit eos doloribus aliquam nam beatae earum? Eum laudantium blanditiis minus vero?</p>\r\n\r\n<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat quia natus velit architecto, quo ab perferendis accusamus modi reprehenderit eos doloribus aliquam nam beatae earum? Eum laudantium blanditiis minus vero?Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat quia natus velit architecto, quo ab perferendis accusamus modi reprehenderit eos doloribus aliquam nam beatae earum? Eum laudantium blanditiis minus vero?</p>\r\n\r\n<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat quia natus velit architecto, quo ab perferendis accusamus modi reprehenderit eos doloribus aliquam nam beatae earum? Eum laudantium blanditiis minus vero?</p>', 10),
(10, 'This is fashion article 1', 'this-is-fashion-article-1', 'images/menu/python-development-service-banner_H5fkFvi.png', '', 1, '2019-08-23 16:39:25.552231', 1, 12, '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam quos magni qui labore suscipit eos, et exercitationem quis itaque quod facilis dolore dignissimos. Perferendis non fuga error, accusamus illum exercitationem.Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam quos magni qui labore suscipit eos, et exercitationem quis itaque quod facilis dolore dignissimos. Perferendis non fuga error, accusamus illum exercitationem.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam quos magni qui labore suscipit eos, et exercitationem quis itaque quod facilis dolore dignissimos. Perferendis non fuga error, accusamus illum exercitationem.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam quos magni qui labore suscipit eos, et exercitationem quis itaque quod facilis dolore dignissimos. Perferendis non fuga error, accusamus illum exercitationem.Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam quos magni qui labore suscipit eos, et exercitationem quis itaque quod facilis dolore dignissimos. Perferendis non fuga error, accusamus illum exercitationem.Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam quos magni qui labore suscipit eos, et exercitationem quis itaque quod facilis dolore dignissimos. Perferendis non fuga error, accusamus illum exercitationem.</p>', 8),
(11, 'This is photography content 1', 'this-is-photography-content-1', 'images/menu/03mw_versailles.jpg', '', 1, '2019-08-23 23:24:48.165446', 1, 14, '', 0),
(12, 'This is photography content 2', 'this-is-photography-content-2', 'images/menu/08f178423a7859c95205cb22d92e42a7.jpg', '', 1, '2019-08-23 23:25:07.966590', 1, 14, '', 0),
(13, 'This is photography content 3', 'this-is-photography-content-3', 'images/menu/105107.jpg', '', 1, '2019-08-23 23:25:18.264467', 1, 14, '', 0),
(14, 'This is photography content 4', 'this-is-photography-content-4', 'images/menu/art-by-lonfeldt-682565-unsplash_banner-560x320.jpg', '', 1, '2019-08-23 23:25:39.463281', 1, 14, '', 0),
(15, 'This is photography content 5', 'this-is-photography-content-5', 'images/menu/gsmarena_001.jpg', '', 1, '2019-08-23 23:25:48.936015', 1, 14, '', 0),
(16, 'This is photography content 6', 'this-is-photography-content-6', 'images/menu/news-kma.jpg', '', 1, '2019-08-23 23:25:58.719969', 1, 14, '', 0),
(17, 'This is photography content 7', 'this-is-photography-content-7', 'images/menu/thumb-1920-695028.jpg', '', 1, '2019-08-23 23:26:07.924929', 1, 14, '', 0),
(18, 'This is photography content 8', 'this-is-photography-content-8', 'images/menu/white-wallpapers-30.jpg', '', 1, '2019-08-23 23:26:17.107456', 1, 14, '', 0),
(19, 'About KU', 'about-ku', '', '', 1, '2019-09-13 15:15:54.907333', 1, 19, '<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Video provides a powerful way to help you prove your point. When you click Online Video, you can paste in the embed code for the video you want to add. You can also type a keyword to search online for the video that best fits your document.</span></span></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">To make your document look professionally produced, Word provides header, footer, cover page, and text box designs that complement each other. For example, you can add a matching cover page, header, and sidebar. Click Insert and then choose the elements you want from the different galleries.</span></span></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Themes and styles also help keep your document coordinated. When you click Design and choose a new Theme, the pictures, charts, and SmartArt graphics change to match your new theme. When you apply styles, your headings change to match the new theme.</span></span></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Save time in Word with new buttons that show up where you need them. To change the way a picture fits in your document, click it and a button for layout options appears next to it. When you work on a table, click where you want to add a row or a column, and then click the plus sign.</span></span></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Reading is easier, too, in the new Reading view. You can collapse parts of the document and focus on the text you want. If you need to stop reading before you reach the end, Word remembers where you left off - even on another device.</span></span></p>\r\n\r\n<p><img alt=\"\" src=\"/media/uploads/ckeditor/2019/09/13/best-programming-languages-for-kids-2-1024x552-1-1024x552.png\" style=\"height:270px; width:500px\" /></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Video provides a powerful way to help you prove your point. When you click Online Video, you can paste in the embed code for the video you want to add. You can also type a keyword to search online for the video that best fits your document.</span></span></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">To make your document look professionally produced, Word provides header, footer, cover page, and text box designs that complement each other. For example, you can add a matching cover page, header, and sidebar. Click Insert and then choose the elements you want from the different galleries.</span></span></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Themes and styles also help keep your document coordinated. When you click Design and choose a new Theme, the pictures, charts, and SmartArt graphics change to match your new theme. When you apply styles, your headings change to match the new theme.</span></span></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Save time in Word with new buttons that show up where you need them. To change the way a picture fits in your document, click it and a button for layout options appears next to it. When you work on a table, click where you want to add a row or a column, and then click the plus sign.</span></span></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Reading is easier, too, in the new Reading view. You can collapse parts of the document and focus on the text you want. If you need to stop reading before you reach the end, Word remembers where you left off - even on another device.</span></span></p>', 33),
(20, 'Prime College', 'prime-college', '', '', 1, '2019-09-13 15:22:04.855031', 1, 20, '<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Video provides a powerful way to help you prove your point. When you click Online Video, you can paste in the embed code for the video you want to add. You can also type a keyword to search online for the video that best fits your document.</span></span></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">To make your document look professionally produced, Word provides header, footer, cover page, and text box designs that complement each other. For example, you can add a matching cover page, header, and sidebar. Click Insert and then choose the elements you want from the different galleries.</span></span></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Themes and styles also help keep your document coordinated. When you click Design and choose a new Theme, the pictures, charts, and SmartArt graphics change to match your new theme. When you apply styles, your headings change to match the new theme.</span></span></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Save time in Word with new buttons that show up where you need them. To change the way a picture fits in your document, click it and a button for layout options appears next to it. When you work on a table, click where you want to add a row or a column, and then click the plus sign.</span></span></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Reading is easier, too, in the new Reading view. You can collapse parts of the document and focus on the text you want. If you need to stop reading before you reach the end, Word remembers where you left off - even on another device.</span></span></p>', 19),
(21, 'Kathmandu Engineering College', 'kathmandu-engineering-college', '', '', 1, '2019-09-13 15:44:12.864564', 1, 20, '<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Video provides a powerful way to help you prove your point. When you click Online Video, you can paste in the embed code for the video you want to add. You can also type a keyword to search online for the video that best fits your document.</span></span></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">To make your document look professionally produced, Word provides header, footer, cover page, and text box designs that complement each other. For example, you can add a matching cover page, header, and sidebar. Click Insert and then choose the elements you want from the different galleries.</span></span></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Themes and styles also help keep your document coordinated. When you click Design and choose a new Theme, the pictures, charts, and SmartArt graphics change to match your new theme. When you apply styles, your headings change to match the new theme.</span></span></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Save time in Word with new buttons that show up where you need them. To change the way a picture fits in your document, click it and a button for layout options appears next to it. When you work on a table, click where you want to add a row or a column, and then click the plus sign.</span></span></p>\r\n\r\n<p><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Reading is easier, too, in the new Reading view. You can collapse parts of the document and focus on the text you want. If you need to stop reading before you reach the end, Word remembers where you left off - even on another device.</span></span></p>\r\n\r\n<ul>\r\n	<li><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Video provides a powerful way to help you prove your point. When you click Online Video, you can paste in the embed code for the video you want to add. You can also type a keyword to search online for the video that best fits your document.</span></span></li>\r\n	<li><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">To make your document look professionally produced, Word provides header, footer, cover page, and text box designs that complement each other. For example, you can add a matching cover page, header, and sidebar. Click Insert and then choose the elements you want from the different galleries.</span></span></li>\r\n	<li><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Themes and styles also help keep your document coordinated. When you click Design and choose a new Theme, the pictures, charts, and SmartArt graphics change to match your new theme. When you apply styles, your headings change to match the new theme.</span></span></li>\r\n	<li><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Save time in Word with new buttons that show up where you need them. To change the way a picture fits in your document, click it and a button for layout options appears next to it. When you work on a table, click where you want to add a row or a column, and then click the plus sign.</span></span></li>\r\n	<li><span style=\"font-size:11pt\"><span style=\"font-family:Calibri,sans-serif\">Reading is easier, too, in the new Reading view. You can collapse parts of the document and focus on the text you want. If you need to stop reading before you reach the end, Word remembers where you left off - even on another device.</span></span></li>\r\n</ul>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `newsapp_newsarticle`
--

CREATE TABLE `newsapp_newsarticle` (
  `id` int(11) NOT NULL,
  `select_type` varchar(10) NOT NULL,
  `name` varchar(200) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `newsapp_newsarticlecategory`
--

CREATE TABLE `newsapp_newsarticlecategory` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsapp_newsarticlecategory`
--

INSERT INTO `newsapp_newsarticlecategory` (`id`, `name`, `slug`, `status`) VALUES
(1, 'Sports', 'sports', 1);

-- --------------------------------------------------------

--
-- Table structure for table `newsapp_popup`
--

CREATE TABLE `newsapp_popup` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `link_name` varchar(255) DEFAULT NULL,
  `order_by` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `created_by_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsapp_popup`
--

INSERT INTO `newsapp_popup` (`id`, `image`, `link`, `link_name`, `order_by`, `status`, `created_at`, `created_by_id`) VALUES
(2, 'images/popup/thumb-1920-695028_Jdn0p4w.jpg', NULL, NULL, 1, 1, '2019-08-29 00:21:21.082259', 1),
(3, 'images/popup/105107.jpg', 'https://www.google.com.np/', 'Click Here', 2, 1, '2019-08-29 09:14:37.882667', 1);

-- --------------------------------------------------------

--
-- Table structure for table `newsapp_quicklinks`
--

CREATE TABLE `newsapp_quicklinks` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `link` varchar(200) NOT NULL,
  `order_by` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `created_by_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsapp_quicklinks`
--

INSERT INTO `newsapp_quicklinks` (`id`, `title`, `link`, `order_by`, `status`, `created_at`, `created_by_id`) VALUES
(1, 'BBC', 'https://www.bbc.com/', 1, 1, '2019-08-11 10:51:43.319174', 1),
(2, 'CNN', 'https://edition.cnn.com/', 2, 1, '2019-08-11 12:58:33.193737', 1),
(3, 'Yahoo News', 'https://news.yahoo.com/', 3, 1, '2019-08-11 12:59:02.589839', 1),
(4, 'Google News', 'https://news.google.com/', 4, 1, '2019-08-11 12:59:24.551020', 1),
(5, 'ABC News', 'https://abcnews.go.com/', 5, 1, '2019-08-11 13:01:18.227449', 1),
(6, 'DailyMail', 'https://www.dailymail.co.uk/home/index.html', 6, 1, '2019-08-11 13:01:48.184297', 1),
(7, 'Fox News', 'https://www.foxnews.com/', 7, 1, '2019-08-11 13:02:04.927978', 1);

-- --------------------------------------------------------

--
-- Table structure for table `newsapp_siteconfig`
--

CREATE TABLE `newsapp_siteconfig` (
  `id` int(11) NOT NULL,
  `select_config` varchar(5) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `link` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsapp_siteconfig`
--

INSERT INTO `newsapp_siteconfig` (`id`, `select_config`, `name`, `description`, `image`, `status`, `link`) VALUES
(1, 'wl', NULL, '', 'images/site_config/logo.png', 1, NULL),
(2, 'wi', NULL, '', 'images/site_config/icon.png', 1, NULL),
(3, 'wt', 'NewsGram', '', '', 1, NULL),
(4, 'ci', NULL, '<p>Baluwatar - 4, Nayabasti Marg<br />\r\nKathmandu, Nepal<br />\r\nContact: +977 9841234567<br />\r\nMail: <a href=\"mailto:info.newsgram@gmail.com\" style=\"color: #c6c6c6\">info.newsgram@gmail.com</a><br />\r\nFax: 123-456-789</p>', '', 1, NULL),
(5, 'ab', NULL, '', 'images/site_config/Advertisement_banner.png', 1, 'https://www.ittrainingnepal.com/');

-- --------------------------------------------------------

--
-- Table structure for table `newsapp_sociallinks`
--

CREATE TABLE `newsapp_sociallinks` (
  `id` int(11) NOT NULL,
  `social_site` varchar(20) NOT NULL,
  `link` varchar(200) NOT NULL,
  `order_by` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `created_by_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsapp_sociallinks`
--

INSERT INTO `newsapp_sociallinks` (`id`, `social_site`, `link`, `order_by`, `status`, `created_at`, `created_by_id`) VALUES
(1, 'facebook', 'https://www.facebook.com/', 1, 1, '2019-08-11 11:35:49.206677', 1),
(2, 'twitter', 'https://www.twitter.com/', 2, 1, '2019-08-11 12:45:38.359858', 1),
(3, 'youtube', 'https://www.youtube.com/', 3, 1, '2019-08-11 13:03:15.762186', 1),
(4, 'gmail', 'https://www.gmail.com/', 4, 1, '2019-08-11 13:03:29.607028', 1);

-- --------------------------------------------------------

--
-- Table structure for table `newsapp_sponsor`
--

CREATE TABLE `newsapp_sponsor` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `order_by` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` datetime(6) NOT NULL,
  `created_by_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsapp_sponsor`
--

INSERT INTO `newsapp_sponsor` (`id`, `title`, `image`, `link`, `order_by`, `status`, `created_at`, `created_by_id`) VALUES
(1, 'Pathao', 'images/sponsor/pathao-app.png', NULL, 1, 1, '2019-08-28 23:06:21.733319', 1),
(2, 'Ncell', 'images/sponsor/Ncell.jpg', NULL, 2, 1, '2019-08-28 23:07:32.856931', 1),
(3, 'Esewa', 'images/sponsor/eSewa-Nepal-App.png', NULL, 3, 1, '2019-08-28 23:07:44.646757', 1),
(4, 'The World Bank', 'images/sponsor/treasury-logo.png', NULL, 4, 1, '2019-08-28 23:07:54.098474', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `newsapp_faq`
--
ALTER TABLE `newsapp_faq`
  ADD PRIMARY KEY (`id`),
  ADD KEY `newsapp_faq_created_by_id_2129f00c_fk_auth_user_id` (`created_by_id`);

--
-- Indexes for table `newsapp_horoscope`
--
ALTER TABLE `newsapp_horoscope`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsapp_imageslider`
--
ALTER TABLE `newsapp_imageslider`
  ADD PRIMARY KEY (`id`),
  ADD KEY `newsapp_imageslider_created_by_id_bd148734_fk_auth_user_id` (`created_by_id`),
  ADD KEY `newsapp_imageslider_slug_700a77a0` (`slug`);

--
-- Indexes for table `newsapp_menu`
--
ALTER TABLE `newsapp_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `newsapp_menu_created_by_id_407eac85_fk_auth_user_id` (`created_by_id`),
  ADD KEY `newsapp_menu_parent_menu_id_2cc04a56_fk_newsapp_menu_id` (`parent_menu_id`),
  ADD KEY `newsapp_menu_slug_fb5a7f0b` (`slug`);

--
-- Indexes for table `newsapp_menucontent`
--
ALTER TABLE `newsapp_menucontent`
  ADD PRIMARY KEY (`id`),
  ADD KEY `newsapp_menucontent_created_by_id_6e02a908_fk_auth_user_id` (`created_by_id`),
  ADD KEY `newsapp_menucontent_menu_id_bdf5989a_fk_newsapp_menu_id` (`menu_id`),
  ADD KEY `newsapp_menucontent_slug_5ef56c19` (`slug`);

--
-- Indexes for table `newsapp_newsarticle`
--
ALTER TABLE `newsapp_newsarticle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `newsapp_newsarticle_slug_89edcda1` (`slug`);

--
-- Indexes for table `newsapp_newsarticlecategory`
--
ALTER TABLE `newsapp_newsarticlecategory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `newsapp_newsarticlecategory_slug_5a0e3c17` (`slug`);

--
-- Indexes for table `newsapp_popup`
--
ALTER TABLE `newsapp_popup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `newsapp_popup_created_by_id_9d97a046_fk_auth_user_id` (`created_by_id`);

--
-- Indexes for table `newsapp_quicklinks`
--
ALTER TABLE `newsapp_quicklinks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `newsapp_quicklinks_created_by_id_dee5755c_fk_auth_user_id` (`created_by_id`);

--
-- Indexes for table `newsapp_siteconfig`
--
ALTER TABLE `newsapp_siteconfig`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsapp_sociallinks`
--
ALTER TABLE `newsapp_sociallinks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `newsapp_sociallinks_created_by_id_ade1d881_fk_auth_user_id` (`created_by_id`);

--
-- Indexes for table `newsapp_sponsor`
--
ALTER TABLE `newsapp_sponsor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `newsapp_sponsor_created_by_id_f3b3780d_fk_auth_user_id` (`created_by_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=212;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `newsapp_faq`
--
ALTER TABLE `newsapp_faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `newsapp_horoscope`
--
ALTER TABLE `newsapp_horoscope`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `newsapp_imageslider`
--
ALTER TABLE `newsapp_imageslider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `newsapp_menu`
--
ALTER TABLE `newsapp_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `newsapp_menucontent`
--
ALTER TABLE `newsapp_menucontent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `newsapp_newsarticle`
--
ALTER TABLE `newsapp_newsarticle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `newsapp_newsarticlecategory`
--
ALTER TABLE `newsapp_newsarticlecategory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `newsapp_popup`
--
ALTER TABLE `newsapp_popup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `newsapp_quicklinks`
--
ALTER TABLE `newsapp_quicklinks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `newsapp_siteconfig`
--
ALTER TABLE `newsapp_siteconfig`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `newsapp_sociallinks`
--
ALTER TABLE `newsapp_sociallinks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `newsapp_sponsor`
--
ALTER TABLE `newsapp_sponsor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `newsapp_faq`
--
ALTER TABLE `newsapp_faq`
  ADD CONSTRAINT `newsapp_faq_created_by_id_2129f00c_fk_auth_user_id` FOREIGN KEY (`created_by_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `newsapp_imageslider`
--
ALTER TABLE `newsapp_imageslider`
  ADD CONSTRAINT `newsapp_imageslider_created_by_id_bd148734_fk_auth_user_id` FOREIGN KEY (`created_by_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `newsapp_menu`
--
ALTER TABLE `newsapp_menu`
  ADD CONSTRAINT `newsapp_menu_created_by_id_407eac85_fk_auth_user_id` FOREIGN KEY (`created_by_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `newsapp_menu_parent_menu_id_2cc04a56_fk_newsapp_menu_id` FOREIGN KEY (`parent_menu_id`) REFERENCES `newsapp_menu` (`id`);

--
-- Constraints for table `newsapp_menucontent`
--
ALTER TABLE `newsapp_menucontent`
  ADD CONSTRAINT `newsapp_menucontent_created_by_id_6e02a908_fk_auth_user_id` FOREIGN KEY (`created_by_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `newsapp_menucontent_menu_id_bdf5989a_fk_newsapp_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `newsapp_menu` (`id`);

--
-- Constraints for table `newsapp_popup`
--
ALTER TABLE `newsapp_popup`
  ADD CONSTRAINT `newsapp_popup_created_by_id_9d97a046_fk_auth_user_id` FOREIGN KEY (`created_by_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `newsapp_quicklinks`
--
ALTER TABLE `newsapp_quicklinks`
  ADD CONSTRAINT `newsapp_quicklinks_created_by_id_dee5755c_fk_auth_user_id` FOREIGN KEY (`created_by_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `newsapp_sociallinks`
--
ALTER TABLE `newsapp_sociallinks`
  ADD CONSTRAINT `newsapp_sociallinks_created_by_id_ade1d881_fk_auth_user_id` FOREIGN KEY (`created_by_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `newsapp_sponsor`
--
ALTER TABLE `newsapp_sponsor`
  ADD CONSTRAINT `newsapp_sponsor_created_by_id_f3b3780d_fk_auth_user_id` FOREIGN KEY (`created_by_id`) REFERENCES `auth_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

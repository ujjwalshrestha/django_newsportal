-- Table: newsapp_imageslider
-- Task: Change columns: description to NULLABLE
-- Ujjwal Shrestha (2019-08-21)
ALTER TABLE newsapp_imageslider CHANGE COLUMN `description` `description` LONGTEXT DEFAULT NULL;
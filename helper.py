"""This page includes all the custom required helper functions.
"""

from django.utils.safestring import mark_safe

def page(page_name):
    """This function returns the path of the file to be rendered.
    
    Arguments:
        page_name {string} -- Name of the page to be rendered.
    """
    
    file_path = 'pages/'
    ext = '.html'
    final_page = file_path + page_name + ext
    return final_page


def form_label(label, font_type):
    if font_type == 'b':
        label = mark_safe(f'<b>{label}</b>')
        
    elif font_type == 'i':
        label = mark_safe(f'<i>{label}</i>')
        
    elif font_type == 'u':
        label = mark_safe(f'<u>{label}</u>')
        
    elif font_type == 'bi' or font_type == 'ib':
        label = mark_safe(f'<b><i>{label}</i></b>')
        
    elif font_type == 'ui' or font_type == 'iu':
        label = mark_safe(f'<u><i>{label}</i></u>')
        
    elif font_type == 'bu' or font_type == 'ub':
        label = mark_safe(f'<u><b>{label}</b></u>')
        
    elif font_type == 'bui' or font_type == 'biu' or font_type == 'ibu' or font_type == 'iub' or font_type == 'uib' or font_type == 'ubi':
        label = mark_safe(f'<i><u><b>{label}</b></u></i>')
        
    return label